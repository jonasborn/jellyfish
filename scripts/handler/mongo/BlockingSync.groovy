package de.obdev.thefink.drummer.handler.implementions.snyc

import de.obdev.thefink.jellyfish.handler.HandlerOutput
import de.obdev.thefink.jellyfish.master.MasterConnections
import de.obdev.thefink.jellyfish.handler.HandlerInput
import de.obdev.thefink.jellyfish.handler.SyncHandler
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage


/**
 * Created by Jonas Born on 21.04.17.
 */

public class ExampleSync extends SyncHandler<MongoMessage> {


    @Override
    HandlerOutput onRequest(HandlerInput<MongoMessage> input) {
        return new HandlerOutput().setBytes(MasterConnections.get().write(input.getBytes()).read());
    }
}
