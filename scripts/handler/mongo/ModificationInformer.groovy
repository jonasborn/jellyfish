import com.github.wnameless.json.flattener.JsonFlattener
import de.obdev.thefink.jellyfish.connection.AbstractConnection
import de.obdev.thefink.jellyfish.connection.Connections
import de.obdev.thefink.jellyfish.connection.JellyfishConnection
import de.obdev.thefink.jellyfish.encoder.MessageEncoders
import de.obdev.thefink.jellyfish.handler.AsyncHandler
import de.obdev.thefink.jellyfish.handler.HandlerInput
import de.obdev.thefink.jellyfish.message.jellyfish.update.UpdateMessage
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage
import de.obdev.thefink.jellyfish.message.mongo.QueryMessage
import org.apache.commons.codec.binary.Hex

import java.util.function.Consumer

public class InformingAsync extends AsyncHandler<MongoMessage> {

    static List<String> updateMarker = new ArrayList<>();
    static {
        updateMarker.add("insert");
        updateMarker.add("update");
    }

    @Override
    void onRequest(HandlerInput<MongoMessage> input) {
        Boolean update = false;
        for (String s:updateMarker) {
            if ((((QueryMessage) input.getMessage()).getQuery().containsKey(s))) {
                update = true;
            }
        }

        if (update) {
            for (AbstractConnection connection:Connections.get(JellyfishConnection.class)) {
                println "Looped"
                println connection.getClass().getName()
                if (connection instanceof JellyfishConnection) {
                    println "Send update message"
                    println input.getMessage().getClass()
                    JellyfishConnection jellyfishConnection = ((JellyfishConnection) connection);
                    println input.getMessage().getClass()
                    try {
                        println Hex.encodeHexString(
                                MessageEncoders.encode(new UpdateMessage(
                                        null,
                                        input.getMessage()
                                ))
                        )
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    println "Here"
                    connection.write(
                            new UpdateMessage(
                                    null,
                                    input.getMessage()
                            )
                    )
                }
            }
        }

    }
}