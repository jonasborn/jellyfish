import de.obdev.thefink.jellyfish.handler.AsyncHandler
import de.obdev.thefink.jellyfish.handler.HandlerInput
import de.obdev.thefink.jellyfish.message.jellyfish.update.UpdateMessage
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage
import de.obdev.thefink.jellyfish.message.mongo.QueryMessage

public class UpdateHandler extends AsyncHandler<UpdateMessage> {

    @Override
    void onRequest(HandlerInput<UpdateMessage> input) {

        println "Received Update Message"

        MongoMessage message = input.getMessage().getMongoMessage();

        println message.getClass()

        if (message instanceof QueryMessage) {
            println message.getQuery();
        }

    }
}