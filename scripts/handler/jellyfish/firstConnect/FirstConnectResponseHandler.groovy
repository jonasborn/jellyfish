import de.obdev.thefink.jellyfish.handler.AsyncHandler
import de.obdev.thefink.jellyfish.handler.HandlerInput
import de.obdev.thefink.jellyfish.message.jellyfish.welcome.FirstConnectResponse

public class ResponseHandler extends AsyncHandler<FirstConnectResponse> {


    @Override
    void onRequest(HandlerInput<FirstConnectResponse> input) {

        println "Received " + input.getMessage().getChange()
    }
}