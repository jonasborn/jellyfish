import de.obdev.thefink.jellyfish.connection.Connections
import de.obdev.thefink.jellyfish.connection.JellyfishConnection
import de.obdev.thefink.jellyfish.handler.AsyncHandler
import de.obdev.thefink.jellyfish.handler.HandlerInput
import de.obdev.thefink.jellyfish.handler.HandlerOutput
import de.obdev.thefink.jellyfish.handler.SyncHandler
import de.obdev.thefink.jellyfish.message.jellyfish.welcome.FirstConnectMessage
import de.obdev.thefink.jellyfish.message.jellyfish.welcome.FirstConnectResponse

public class WelcomeHandler extends SyncHandler<FirstConnectMessage> {

    @Override
    HandlerOutput onRequest(HandlerInput<FirstConnectMessage> input) {

        if (input.getConnection() instanceof JellyfishConnection) {
            ((JellyfishConnection) input.getConnection()).setOwner(
                    input.getMessage().getSender()
            )
        }

        Integer current = Connections.count(JellyfishConnection.class)
        Boolean change = false;
        Integer diff = Math.abs(current-input.getMessage().getDifference());

        if (diff>3) {
            change = true;
        }
        return new HandlerOutput().setBytes(
                new FirstConnectResponse(input.getMessage().getSender(), change).toByteArray()
        );
    }
}