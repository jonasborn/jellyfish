import de.obdev.thefink.jellyfish.handler.AsyncHandler
import de.obdev.thefink.jellyfish.handler.HandlerInput
import de.obdev.thefink.jellyfish.message.jellyfish.status.UpdatingStatusMessage
import de.obdev.thefink.jellyfish.system.SysInfo
import de.obdev.thefink.jellyfish.system.SysInfos
import de.obdev.thefink.jellyfish.util.join.Joiner

public class UpdateStatusMessageHandler extends AsyncHandler<UpdatingStatusMessage> {

    @Override
    void onRequest(HandlerInput<UpdatingStatusMessage> input) {
        SysInfo sysInfo = SysInfos.get(input.getMessage().getSender());
        Joiner.join(sysInfo, input.getMessage().getSysInfo());
    }
}