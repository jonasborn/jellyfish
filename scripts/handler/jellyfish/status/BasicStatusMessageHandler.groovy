import de.obdev.thefink.jellyfish.handler.AsyncHandler
import de.obdev.thefink.jellyfish.handler.HandlerInput
import de.obdev.thefink.jellyfish.message.jellyfish.status.BasicStatusMessage
import de.obdev.thefink.jellyfish.system.SysInfos
import org.apache.logging.log4j.LogManager

public class BasicStatusMessageHandler extends AsyncHandler<BasicStatusMessage> {



    @Override
    void onRequest(HandlerInput<BasicStatusMessage> input) {

        SysInfos.set(input.getMessage().getSender(), input.getMessage().getSysInfo());
    }
}