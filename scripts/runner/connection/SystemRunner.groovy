import de.obdev.thefink.jellyfish.runner.Runner
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

public class SystemRunner extends Runner {

    Logger logger = LogManager.getLogger();


    @Override
    void start() {
        logger.info("Started example runner")
    }

    @Override
    void run() {
        logger.info("Example runner is running")
    }

    @Override
    void stop() {
        logger.info("Stopped example runner")
    }
}