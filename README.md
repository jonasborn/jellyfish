# Jellyfish
Jellyfish is a stable, extensible and decentralized proxy for MongoDB
  - Able to replicate only single databases
  - Fallback and loadbalancing through own client
  - Able to inform clients about changes
  - Able to communicate with other instances
  - Able to intelligent cache documents with self learning rules

# In development
This project is still being finalized, but it already offers solid foundations:
- Parsing the documents already works
- Calling the handlers (from scripts) already works
- Communication between different entities is already working

So if you want to have a fast solution for temporary storage, you can already make quick successes; Simply add the appropriate Groovy files

But I can not promise that everything works 100%

The client, also known as Butterfly, is still not at all ready, and not yet implemented.

# Construction
The proxy is structured by the code quite abstractly, there are in the whole 3 mostly important classes:
Mongo *
Jellyfish *
Butterfly*
Ie. We have something like MongoServer, JellyfishServer, etc., all of which expand an abstract class and basically work the same, just include different parsers and call different handlers
The same applies to the clients (MongoClient, JellyfishClient, ...), as with the parsers

# Functionality
## Example at MongoDB
### SyncHandler
The proxy always accepts sockets from clients and then creates new connections to the database (one db connection for each incomming).
Each link is then divided into individual packets which, after passing through the parser, are passed on to the appropriate handlers. The handlers can then process the content and respond with their own content or content from the server.

```
                       RESPONSE DIRECT OR
                       REDIRECT TO DB
 CLIENT IN <--+ SENDER <---+--+  +-------------------+
                           +  |  v
                           HANDLER     HANDLE RESULT AND SEND RESPONSE
                           ^  |
CLIENT OUT +--> PARSER +---+  +---------------------->
```

Here a small example for direct answering, without change
```java
public class ExampleSync extends SyncHandler<MongoMessage> {
    @Override
    HandlerOutput onRequest(HandlerInput<MongoMessage> input) {
        return new HandlerOutput().setBytes(MasterConnections.get().write(input.getBytes()).read());
    }
}
```
As you can see, there is simply SyncHandler up there with the document to handle.
Unfortunately, I'm just not sure what happens when several SyncHandlers are present. But I guess that, as soon as one does not give zero, the same responds.

The construction remains, as already said, always the same, for all connections.
### AsyncHandler
Oh, I have also added a way for asynchronous handlers, which, unlike the synchronous ones, can not manipulate the output to the client.
You can, of course, still pull data from the database and do other great things, e.g. Inform other clients about data being modified, etc.
Always well-balanced between the possibilities that block asynchronous namely nothing
They are built like the others, only without return possibility.

``` java
public class UpdateHandler extends AsyncHandler<UpdateMessage> {
    @Override
    void onRequest(HandlerInput<UpdateMessage> input) {
    }
}
```

### Runners
As it seems I have also installed a possibility for so-called runner. These can, as their name implies, run, doing different tasks. Either as a loop or once.
The great: you still can edit them while the program is running.
``` java
public class SystemRunner extends Runner {
    Logger logger = LogManager.getLogger();
    @Override
    void start() {
        logger.info("Started example runner")
    }
    @Override
    void run() {
        logger.info("Example runner is running")
    }
    @Override
    void stop() {
        logger.info("Stopped example runner")
    }
}
```
Please use the run function if you want to run anything in a while loop ;)

# Tech
## Parsing
Only as a small hint, how I have decomposed the documents:
I know that because of the badly documented measures around the WireProtocoll, it can be really hard to get that out. I convert them with the functions in the class de.obdev.thefink.jellyfish.util.mongo.document.MongoDocument, with the help of the internal functions of the driver. Unfortunately you have to assemble the documents correctly; This works best with the information from WireShark
It really took forever to get to the running ... And yes, I've googled ;)
#### Logging
Yea, I'm a fan of log4j2, sorry about that.
#### Other
Want to know what I used? Look in the pom.xml.


# Installation

Jellyfish requires [Java](https://www.java.com/) 8 to run. I'm not sure if it works with OpenJDK, I prefer (and because I had some problems) with the version of Oracle.

Maven is also a very good idea, of course you could download all jars individually, much fun with it

# Development
Want to contribute? Great!
I've never tryed it with BitBucket, but hey, theres always a firs try.
As you can see everything is based on Git and GitFlow

# Questions?
Contact me on [Twitter](https://twitter.com/OrangeblockDev) ;D

@Jonas Born, 2017
