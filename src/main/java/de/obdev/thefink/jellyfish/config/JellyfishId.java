package de.obdev.thefink.jellyfish.config;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import oshi.SystemInfo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.SocketException;

/**
 * Created by Jonas Born on 30.04.17.
 */
public class JellyfishId implements Serializable {

    static Kryo kryo = new Kryo();
    static Logger logger = LogManager.getLogger();
    static byte[] currentMac;
    static JellyfishId ownId;

    static {
        SystemInfo systemInfo = new SystemInfo();
        try {
            currentMac = systemInfo.getHardware().getNetworkIFs()[0].getNetworkInterface().getHardwareAddress();
        } catch (SocketException e) {
            e.printStackTrace();
        }

        String ip = Configs.current().getJellyfishConfig().getHost();
        Integer port = Configs.current().getJellyfishConfig().getPort();
        byte[] mac = currentMac;
        byte[] rand = Configs.current().getJellyfishConfig().getRandom();
        ownId = new JellyfishId(ip, port, mac, rand);

        logger.info("Hey, I'm {}", ownId.toString());

    }

    String ip;
    Integer port;
    byte[] mac;
    byte[] rand = new byte[16];
    public JellyfishId(String ip, Integer port, byte[] mac, byte[] rand) {
        this.ip = ip;
        this.port = port;
        this.mac = mac;
        this.rand = rand;
    }

    public JellyfishId() {
        if (ownId != null) {
            this.ip = ownId.getIp();
            this.port = ownId.getPort();
            this.mac = ownId.getMac();
            this.rand = ownId.getRand();
        }
    }

    public static JellyfishId getId() {
        return ownId;
    }

    public static void start() {

    }

    public static JellyfishId fromString(String s) throws IOException, ClassNotFoundException {
        return JellyfishId.fromByteArray(Base64.decodeBase64(s));
    }

    public static JellyfishId fromByteArray(byte[] bytes) throws IOException, ClassNotFoundException {
        Input input = new Input(new ByteArrayInputStream(bytes));
        return kryo.readObject(input, JellyfishId.class);
    }

    public byte[] toByteArray() throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        Output output = new Output(os);
        kryo.writeObject(output, this);
        output.close();
        byte[] res = os.toByteArray();
        os.close();
        return res;
    }

    public String getIp() {
        return ip;
    }

    public Integer getPort() {
        return port;
    }

    public byte[] getMac() {
        return mac;
    }

    public byte[] getRand() {
        return rand;
    }

    @Override
    public String toString() {
        try {
            return Base64.encodeBase64URLSafeString(this.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
