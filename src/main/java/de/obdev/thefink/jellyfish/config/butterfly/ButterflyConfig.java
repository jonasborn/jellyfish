package de.obdev.thefink.jellyfish.config.butterfly;

/**
 * Created by Jonas Born on 30.04.17.
 */
public class ButterflyConfig {

    Integer port;

    public Integer getPort() {
        return port;
    }

    public ButterflyConfig setPort(Integer port) {
        this.port = port;
        return this;
    }
}
