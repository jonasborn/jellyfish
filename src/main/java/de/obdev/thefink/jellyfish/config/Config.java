package de.obdev.thefink.jellyfish.config;

import de.obdev.thefink.jellyfish.config.butterfly.ButterflyConfig;
import de.obdev.thefink.jellyfish.config.jellyfish.JellyfishConfig;
import de.obdev.thefink.jellyfish.config.mongo.MongoConfig;

/**
 * Created by Jonas Born on 30.04.17.
 */
public class Config {

    JellyfishConfig jellyfishConfig;
    ButterflyConfig butterflyConfig;
    MongoConfig mongoConfig;

    public JellyfishConfig getJellyfishConfig() {
        return jellyfishConfig;
    }

    public ButterflyConfig getButterflyConfig() {
        return butterflyConfig;
    }

    public MongoConfig getMongoConfig() {
        return mongoConfig;
    }

    public Config setJellyfishConfig(JellyfishConfig jellyfishConfig) {
        this.jellyfishConfig = jellyfishConfig;
        return this;
    }

    public Config setButterflyConfig(ButterflyConfig butterflyConfig) {
        this.butterflyConfig = butterflyConfig;
        return this;
    }

    public Config setMongoConfig(MongoConfig mongoConfig) {
        this.mongoConfig = mongoConfig;
        return this;
    }
}
