package de.obdev.thefink.jellyfish.config.mongo;

import com.mongodb.ServerAddress;

/**
 * Created by Jonas Born on 18.04.17.
 */
public class ConfigAddress {

    String host;
    Integer port;

    public ConfigAddress(String host, Integer port) {
        this.host = host;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public Integer getPort() {
        return port;
    }

    public ServerAddress convert() {
        return new ServerAddress(host, port);
    }
}
