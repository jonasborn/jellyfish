package de.obdev.thefink.jellyfish.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.obdev.thefink.jellyfish.config.butterfly.ButterflyConfig;
import de.obdev.thefink.jellyfish.config.jellyfish.JellyfishConfig;
import de.obdev.thefink.jellyfish.config.mongo.ConfigAddress;
import de.obdev.thefink.jellyfish.config.mongo.MongoConfig;
import de.obdev.thefink.jellyfish.util.gson.ByteArrayAdapter;
import de.obdev.thefink.jellyfish.util.gson.GBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

/**
 * Created by Jonas Born on 18.04.17.
 */
public class Configs {

    static Logger logger = LogManager.getLogger();

    static File configFile = new File("config.json");

    static Gson gson = new GsonBuilder()
            .registerTypeAdapter(byte[].class, new ByteArrayAdapter())
            .setPrettyPrinting()
            .create();

    static Config current;

    /*
    static {

        Config config = new Config();
        config.setButterflyConfig(new ButterflyConfig().setPort(29027));
        JellyfishConfig jellyfishConfig = new JellyfishConfig();

        byte[] bytes = new byte[16];
        new Random().nextBytes(bytes);
        jellyfishConfig.setRandom(bytes);
        jellyfishConfig.setPort(29017);


        config.setJellyfishConfig(jellyfishConfig);
        config.setMongoConfig(
                new MongoConfig()
                        .setAddress(
                                new ConfigAddress(
                                        "149.202.162.119", 27017
                                )
                        )
        );

        try {
            store(config);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            read();
        } catch (Exception e) {
            logger.fatal("Unable to read config on startup: {}", e);
        }
    }
    */

    public static void setFile(File file) {
        configFile = file;
    }

    public static void read() throws FileNotFoundException {
        current = gson.fromJson(new FileReader(configFile), Config.class);
        JellyfishId.start();
    }

    public static void store(Config config) throws IOException {
        String json = gson.toJson(config);
        FileUtils.writeStringToFile(configFile, json, "UTF-8");
    }

    public static Config current() {
        return current;
    }


}
