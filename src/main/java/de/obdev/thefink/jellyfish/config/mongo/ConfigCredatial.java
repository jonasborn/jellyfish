package de.obdev.thefink.jellyfish.config.mongo;

import com.mongodb.MongoCredential;

/**
 * Created by Jonas Born on 18.04.17.
 */
public class ConfigCredatial {

    String username;
    String database;
    String password;

    public ConfigCredatial(String username, String database, String password) {
        this.username = username;
        this.database = database;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getDatabase() {
        return database;
    }

    public String getPassword() {
        return password;
    }

    public MongoCredential convert() {
        return MongoCredential.createCredential(username, database, password.toCharArray());
    }
}
