package de.obdev.thefink.jellyfish.config.mongo;

import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jonas Born on 30.04.17.
 */
public class MongoConfig {

    Integer port;
    ConfigAddress address;
    List<ConfigCredatial> credentials;

    public Integer getPort() {
        return port;
    }

    public ServerAddress getAddress() {
        return address.convert();
    }

    public List<MongoCredential> getCredatials() {
        List<MongoCredential> list = new ArrayList<>();
        credentials.forEach((c) -> list.add(c.convert()));
        return list;
    }

    public MongoConfig setAddress(ConfigAddress address) {
        this.address = address;
        return this;
    }

    public MongoConfig setCredentials(List<ConfigCredatial> credentials) {
        this.credentials = credentials;
        return this;
    }
}
