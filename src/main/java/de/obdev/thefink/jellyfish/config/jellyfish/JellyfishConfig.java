package de.obdev.thefink.jellyfish.config.jellyfish;

import de.obdev.thefink.jellyfish.config.JellyfishId;

import java.util.List;

/**
 * Created by Jonas Born on 30.04.17.
 */
public class JellyfishConfig {

    String host;
    Integer port;
    byte[] random;

    List<JellyfishId> jellyfishs;


    public List<JellyfishId> getJellyfishs() {
        return jellyfishs;
    }

    public JellyfishConfig setHost(String host) {
        this.host = host;
        return this;
    }

    public JellyfishConfig setPort(Integer port) {
        this.port = port;
        return this;
    }

    public JellyfishConfig setRandom(byte[] random) {
        this.random = random;
        return this;
    }

    public Integer getPort() {
        return port;
    }

    public String getHost() {
        return host;
    }


    public byte[] getRandom() {
        return random;
    }

    public JellyfishConfig setJellyfishs(List<JellyfishId> jellyfishs) {
        this.jellyfishs = jellyfishs;
        return this;
    }
}
