package de.obdev.thefink.jellyfish.action;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Set;

/**
 * Created by Jonas Born on 26.04.17.
 */
public class Actions {

    static Logger logger = LogManager.getLogger();

    public static void start() throws InvocationTargetException, IllegalAccessException {

        print();

        Reflections reflections = new Reflections("", new MethodAnnotationsScanner());
        Set<Method> methods = reflections.getMethodsAnnotatedWith(Startable.class);
        for (Method m:methods) {
            m.invoke(null, null);
            logger.warn("Started {}", m);
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void print() {
        System.out.println(
                "         `:/+o+/-         \n" +
                "     `+dMMMMMMMMMh/      \n" +
                "    /NMMMMMMMMMMMMMm:    \n" +
                "   +MMhos+hMMMyos+hMM:   \n" +
                "  `NMMNMMMNMMMMMMMMMMd   \n" +
                "  .MMMMMMMsos+mMMMMMMN   \n" +
                "   oNMMMMMMMMMMMMMMMm:   \n" +
                "     .:ds--/m/--yh-.     \n" +
                "      s+   h:    s/      \n" +
                "      d.   so    o+      \n" +
                "      /y    +y.`oy       \n" +
                "       :y: -oy-m-`       \n" +
                "      ..`m.    :o+`      \n" +
                "      `/+-               \n");


    }

}
