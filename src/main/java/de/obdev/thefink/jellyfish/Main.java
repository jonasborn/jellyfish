package de.obdev.thefink.jellyfish;

import com.mashape.unirest.http.exceptions.UnirestException;
import de.obdev.thefink.jellyfish.action.Actions;
import de.obdev.thefink.jellyfish.config.Configs;
import de.obdev.thefink.jellyfish.handler.Handlers;
import de.obdev.thefink.jellyfish.util.thread.ThreadManager;
import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import javassist.NotFoundException;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by Jonas Born on 26.04.17.
 */
public class Main {

    public static void main(String[] args) throws ClassNotFoundException, InterruptedException, InvocationTargetException, IllegalAccessException, IOException, UnirestException, NotFoundException {
        Configurator.setRootLevel(Level.DEBUG);


        Configurator.setRootLevel(Level.INFO);

        Configs.setFile(new File("jelly1.json"));
        Configs.read();


        Actions.start();

        Handlers.start();

        Thread.sleep(4000);


        System.out.println(
                "         `:/+o+/-         \n" +
                        "     `+dMMMMMMMMMh/      \n" +
                        "    /NMMMMMMMMMMMMMm:    \n" +
                        "   +MMMMMMMMMMMMMMMMM:   \n" +
                        "  `NMMNMMMNMMMMMMMMMMd   \n" +
                        "  .MMMMMMMMMMMMMMMMMMN   \n" +
                        "   oNMMMMMMMMMMMMMMMm:   \n" +
                        "     .:ds--/m/--yh-.     \n" +
                        "      s+   h:    s/      \n" +
                        "      d.   so    o+      \n" +
                        "      /y    +y.`oy       \n" +
                        "       :y: -oy-m-`       \n" +
                        "      ..`m.    :o+`      \n" +
                        "      `/+-               ");


    }

}
