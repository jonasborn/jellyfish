package de.obdev.thefink.jellyfish.message.mongo;



import de.obdev.thefink.jellyfish.util.mongo.document.MongoDocument;

import java.util.List;

/**
 * Created by Jonas Born on 21.04.17.
 */
public class CommandReplyMessage extends MongoMessage {

    MongoDocument metadata;
    MongoDocument commandReply;
    List<MongoDocument> outputDocs;

    public CommandReplyMessage(MessageHeader header, MongoDocument metadata, MongoDocument commandReply, List<MongoDocument> outputDocs) {
        super(header);
        this.metadata = metadata;
        this.commandReply = commandReply;
        this.outputDocs = outputDocs;
    }

    public MongoDocument getMetadata() {
        return metadata;
    }

    public MongoDocument getCommandReply() {
        return commandReply;
    }

    public List<MongoDocument> getOutputDocs() {
        return outputDocs;
    }
}



