package de.obdev.thefink.jellyfish.message.jellyfish.welcome;


import de.obdev.thefink.jellyfish.config.JellyfishId;
import de.obdev.thefink.jellyfish.connection.Connections;
import de.obdev.thefink.jellyfish.message.jellyfish.JellyfishMessage;

import de.obdev.thefink.jellyfish.config.JellyfishId;

/**
 * Created by Jonas Born on 23.04.17.
 */
public class FirstConnectResponse extends JellyfishMessage {

    private Integer difference;
    private Boolean change = false;

    public FirstConnectResponse(JellyfishId receiver, Boolean change) {
        super(receiver);
        this.change = change;
        this.difference = Connections.count(JellyfishMessage.class);
    }

    public Integer getDifference() {
        return difference;
    }

    public Boolean getChange() {
        return change;
    }
}
