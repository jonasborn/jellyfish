package de.obdev.thefink.jellyfish.message.mongo;


import de.obdev.thefink.jellyfish.util.mongo.document.MongoDocument;

/**
 * Created by Jonas Born on 20.04.17.
 */
public class QueryMessage extends MongoMessage {

    Integer flags;
    String fullCollectionName;
    Integer toSkip;
    Integer toReturn;
    MongoDocument query;

    public QueryMessage(MessageHeader header, Integer flags, String fullCollectionName, Integer toSkip, Integer toReturn, MongoDocument query) {
        super(header);
        this.flags = flags;
        this.fullCollectionName = fullCollectionName;
        this.toSkip = toSkip;
        this.toReturn = toReturn;
        this.query = query;
    }

    public Integer getFlags() {
        return flags;
    }

    public String getFullCollectionName() {
        return fullCollectionName;
    }

    public Integer getToSkip() {
        return toSkip;
    }

    public Integer getToReturn() {
        return toReturn;
    }

    public MongoDocument getQuery() {
        return query;
    }
}
