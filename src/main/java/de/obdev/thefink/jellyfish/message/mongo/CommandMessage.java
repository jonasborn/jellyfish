package de.obdev.thefink.jellyfish.message.mongo;



import de.obdev.thefink.jellyfish.util.mongo.document.MongoDocument;

import java.util.List;

/**
 * Created by Jonas Born on 21.04.17.
 */
public class CommandMessage extends MongoMessage {

    String database;
    String commandName;
    MongoDocument metadata;
    MongoDocument commandArgs;
    List<MongoDocument> inputDocs;

    public CommandMessage(MessageHeader header, String database, String commandName, MongoDocument metadata, MongoDocument commandArgs, List<MongoDocument> inputDocs) {
        super(header);
        this.database = database;
        this.commandName = commandName;
        this.metadata = metadata;
        this.commandArgs = commandArgs;
        this.inputDocs = inputDocs;
    }

    public String getDatabase() {
        return database;
    }

    public String getCommandName() {
        return commandName;
    }

    public MongoDocument getMetadata() {
        return metadata;
    }

    public MongoDocument getCommandArgs() {
        return commandArgs;
    }

    public List<MongoDocument> getInputDocs() {
        return inputDocs;
    }
}
