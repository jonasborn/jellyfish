package de.obdev.thefink.jellyfish.message.mongo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jonas Born on 20.04.17.
 */
public enum OpCode implements Serializable {


    REPLY(1),
    MSG(1000),
    UPDATE(2001),
    INSERT(2002),
    RESERVED(2003),
    QUERY(2004),
    GET_MORE(2005),
    DELETE(2006),
    KILL_CURSORS(2007),
    COMMAND(2010),
    COMMANDREPLY(2011);
    Integer opCode;
    static Map<Integer, OpCode> opCodes = new HashMap<>();

    static {
        for (OpCode t : OpCode.values()) {
            opCodes.put(t.getOpCode(), t);
        }
    }

    OpCode(int opCode) {
        this.opCode = opCode;
    }

    public Integer getOpCode() {
        return opCode;
    }

    public static OpCode getByOpCode(Integer i) {
        return opCodes.get(i);
    }

}
