package de.obdev.thefink.jellyfish.message.mongo;

/**
 * Created by Jonas Born on 21.04.17.
 */
public class MSGMessage extends MongoMessage {

    String message;

    public MSGMessage(MessageHeader header, String message) {
        super(header);
        this.message = message;
    }
}
