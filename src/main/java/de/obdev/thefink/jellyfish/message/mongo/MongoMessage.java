package de.obdev.thefink.jellyfish.message.mongo;

import de.obdev.thefink.jellyfish.message.AbstractMessage;

/**
 * Created by Jonas Born on 20.04.17.
 */
public  abstract class MongoMessage extends AbstractMessage {

    MessageHeader header;

    public MongoMessage() {
    }

    public MongoMessage(MessageHeader header) {
        this.header = header;
    }

    public MessageHeader getHeader() {
        return header;
    }


}

