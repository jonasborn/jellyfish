package de.obdev.thefink.jellyfish.message.jellyfish.welcome;


import de.obdev.thefink.jellyfish.config.JellyfishId;
import de.obdev.thefink.jellyfish.connection.Connections;
import de.obdev.thefink.jellyfish.message.jellyfish.JellyfishMessage;


/**
 * Created by Jonas Born on 23.04.17.
 */
public class FirstConnectMessage extends JellyfishMessage {

    private Integer difference;

    public FirstConnectMessage(JellyfishId receiver) {
        super(receiver);
        difference = Connections.count(JellyfishMessage.class);
    }

    public Integer getDifference() {
        return difference;
    }
}
