package de.obdev.thefink.jellyfish.message.mongo;

import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by Jonas Born on 20.04.17.
 */
public class MessageHeader implements Serializable {

    Integer length;
    Integer requestId;
    Integer responseTo;
    OpCode opCode;

    public MessageHeader(ByteBuffer buffer) {
        length = buffer.getInt();
        requestId = buffer.getInt();
        responseTo = buffer.getInt();
        opCode = OpCode.getByOpCode(buffer.getInt());
    }

    public Integer getLength() {
        return length;
    }

    public Integer getRequestId() {
        return requestId;
    }

    public Integer getResponseTo() {
        return responseTo;
    }

    public OpCode getOpCode() {
        return opCode;
    }

    public byte[] toByteArray(byte[] body) throws IOException {

        ByteBuffer buffer = ByteBuffer.allocate(16 + body.length);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer.putInt(16 + body.length);
        buffer.putInt(requestId);
        buffer.putInt(responseTo);
        buffer.putInt(opCode.getOpCode());
        buffer.put(body);
        byte[] data = new byte[buffer.capacity()];
        buffer.position(0);
        buffer.get(data);
        return data;
    }

}
