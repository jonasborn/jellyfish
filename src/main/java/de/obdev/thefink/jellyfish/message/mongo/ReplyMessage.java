package de.obdev.thefink.jellyfish.message.mongo;



import de.obdev.thefink.jellyfish.util.mongo.document.MongoDocument;

import java.util.List;

/**
 * Created by Jonas Born on 20.04.17.
 */
public class ReplyMessage extends MongoMessage {

    private Integer flags;
    private Long cursorId;
    private Integer from;
    private Integer to;
    private Integer returned;
    private List<MongoDocument> document;

    public ReplyMessage(MessageHeader header, Integer flags, Long cursorId, Integer from, Integer to, Integer returned, List<MongoDocument> document) {
        super(header);
        this.flags = flags;
        this.cursorId = cursorId;
        this.from = from;
        this.to = to;
        this.returned = returned;
        this.document = document;
    }

    public Integer getFlags() {
        return flags;
    }

    public Long getCursorId() {
        return cursorId;
    }

    public Integer getFrom() {
        return from;
    }

    public Integer getTo() {
        return to;
    }

    public Integer getReturned() {
        return returned;
    }

    public List<MongoDocument> getDocument() {
        return document;
    }
}
