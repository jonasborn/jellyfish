package de.obdev.thefink.jellyfish.message.jellyfish.status;

import de.obdev.thefink.jellyfish.config.JellyfishId;
import de.obdev.thefink.jellyfish.message.jellyfish.JellyfishMessage;
import de.obdev.thefink.jellyfish.system.SysInfo;
import de.obdev.thefink.jellyfish.system.SysInfos;

/**
 * Created by Jonas Born on 11.05.17.
 */
public class BasicStatusMessage extends JellyfishMessage {

    SysInfo sysInfo;

    public BasicStatusMessage(JellyfishId receiver) {
        super(receiver);
        sysInfo = SysInfos.current();
    }

    public SysInfo getSysInfo() {
        return sysInfo;
    }
}
