package de.obdev.thefink.jellyfish.message.mongo;

/**
 * Created by Jonas Born on 20.04.17.
 */
public class GetMoreMessage extends MongoMessage {


    Integer zero;
    String fullCollectionName;
    Integer toReturn;
    Long cursorId;

    public GetMoreMessage(MessageHeader header, Integer zero, String fullCollectionName, Integer toReturn, Long cursorId) {
        super(header);
        this.zero = zero;
        this.fullCollectionName = fullCollectionName;
        this.toReturn = toReturn;
        this.cursorId = cursorId;
    }

    public Integer getZero() {
        return zero;
    }

    public String getFullCollectionName() {
        return fullCollectionName;
    }

    public Integer getToReturn() {
        return toReturn;
    }

    public Long getCursorId() {
        return cursorId;
    }
}
