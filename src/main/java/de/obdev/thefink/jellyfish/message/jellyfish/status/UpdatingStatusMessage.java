package de.obdev.thefink.jellyfish.message.jellyfish.status;

import de.obdev.thefink.jellyfish.config.JellyfishId;
import de.obdev.thefink.jellyfish.message.jellyfish.JellyfishMessage;
import de.obdev.thefink.jellyfish.system.SysInfo;
import de.obdev.thefink.jellyfish.system.SysInfos;
import de.obdev.thefink.jellyfish.system.memory.Memory;
import de.obdev.thefink.jellyfish.system.processor.Processor;

/**
 * Created by Jonas Born on 11.05.17.
 */
public class UpdatingStatusMessage extends JellyfishMessage {

    SysInfo sysInfo;

    public UpdatingStatusMessage(JellyfishId receiver) {
        super(receiver);
        Memory memory = new Memory();
        memory.setSwapUsed();
        memory.setAvailable();

        Processor processor = new Processor();
        processor.setSystemCpuLoad();
        processor.setSystemUptime();

        sysInfo = new SysInfo().setMemory(memory).setProcessor(processor);
    }

    public SysInfo getSysInfo() {
        return sysInfo;
    }
}
