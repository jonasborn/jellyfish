package de.obdev.thefink.jellyfish.message.mongo;


import de.obdev.thefink.jellyfish.util.mongo.document.MongoDocument;

/**
 * Created by Jonas Born on 20.04.17.
 */
public class DeleteMessage extends MongoMessage {

    Integer zero;
    String fullCollectionName;
    Integer flags;
    MongoDocument selector;

    public DeleteMessage(MessageHeader header, Integer zero, String fullCollectionName, Integer flags, MongoDocument selector) {
        super(header);
        this.zero = zero;
        this.fullCollectionName = fullCollectionName;
        this.flags = flags;
        this.selector = selector;
    }

    public Integer getZero() {
        return zero;
    }

    public String getFullCollectionName() {
        return fullCollectionName;
    }

    public Integer getFlags() {
        return flags;
    }

    public MongoDocument getSelector() {
        return selector;
    }
}
