package de.obdev.thefink.jellyfish.message;

import de.obdev.thefink.jellyfish.encoder.MessageEncoders;

import java.io.Serializable;

/**
 * Created by Jonas Born on 22.04.17.
 */
public abstract class AbstractMessage implements Serializable {

    public AbstractMessage() {
    }

    public byte[] toByteArray() throws ClassNotFoundException {
        return MessageEncoders.encode(this);
    }

}
