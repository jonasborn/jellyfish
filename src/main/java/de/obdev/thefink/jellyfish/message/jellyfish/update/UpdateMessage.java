package de.obdev.thefink.jellyfish.message.jellyfish.update;

import de.obdev.thefink.jellyfish.config.JellyfishId;
import de.obdev.thefink.jellyfish.message.jellyfish.JellyfishMessage;
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage;

/**
 * Created by Jonas Born on 01.05.17.
 */
public class UpdateMessage extends JellyfishMessage {
    MongoMessage mongoMessage;

    public UpdateMessage(JellyfishId receiver, MongoMessage mongoMessage) {
        super(receiver);
        this.mongoMessage = mongoMessage;
    }

    public MongoMessage getMongoMessage() {
        return mongoMessage;
    }
}
