package de.obdev.thefink.jellyfish.message.jellyfish;


import de.obdev.thefink.jellyfish.config.JellyfishId;
import de.obdev.thefink.jellyfish.message.AbstractMessage;

import java.util.Date;

/**
 * Created by Jonas Born on 18.04.17.
 */
public abstract class JellyfishMessage extends AbstractMessage {

    private JellyfishId sender;
    private JellyfishId receiver;
    private Date start;

    public JellyfishMessage(JellyfishId receiver) {
        sender = JellyfishId.getId();
        start = new Date();
        this.receiver = receiver;
    }

    public JellyfishMessage setSender(JellyfishId sender) {
        this.sender = sender;
        return this;
    }

    public JellyfishId getSender() {
        return sender;
    }

    public Date getStart() {
        return start;
    }

    public JellyfishId getReceiver() {
        return receiver;
    }

    @Override
    public String toString() {
        return "JellyfishMessage{" +
                "sender=" + sender +
                ", receiver=" + receiver +
                ", start=" + start +
                '}';
    }
}
