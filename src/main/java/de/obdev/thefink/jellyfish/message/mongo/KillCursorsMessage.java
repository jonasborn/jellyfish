package de.obdev.thefink.jellyfish.message.mongo;

/**
 * Created by Jonas Born on 21.04.17.
 */
public class KillCursorsMessage extends MongoMessage {

    Integer zero;
    Integer numberOfCursorIds;
    Long cursorsIds;

    public KillCursorsMessage(MessageHeader header) {
        super(header);
    }

    public KillCursorsMessage(MessageHeader header, Integer zero, Integer numberOfCursorIds, Long cursorsIds) {
        super(header);
        this.zero = zero;
        this.numberOfCursorIds = numberOfCursorIds;
        this.cursorsIds = cursorsIds;
    }
}

