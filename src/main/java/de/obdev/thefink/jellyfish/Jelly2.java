package de.obdev.thefink.jellyfish;

import de.obdev.thefink.jellyfish.action.Actions;
import de.obdev.thefink.jellyfish.client.JellyfishClient;
import de.obdev.thefink.jellyfish.config.Configs;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by Jonas Born on 01.05.17.
 */
public class Jelly2 {

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException, InterruptedException, IOException, ClassNotFoundException {

        Configurator.setRootLevel(Level.INFO);

        Configs.setFile(new File("jelly2.json"));
        Configs.read();

        Actions.start();

        JellyfishClient jfc = new JellyfishClient("AQExMjcuMC4wLrEBB9ypcU01eAHCyAMBEYd80d8wAA0f9gltqHfPJiM");
        jfc.start();

    }

}
