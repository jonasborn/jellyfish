package de.obdev.thefink.jellyfish.util.file;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jonas Born on 16.04.17.
 */
public class ChangeDetector {

    static Map<File, FileAlterationObserver> observer = new HashMap<>();
    static Map<File, FileAlterationMonitor> monitors = new HashMap<>();


    public static void detect(File folder, FileAlterationListener listener) {

        FileAlterationListener changeListener = listener;

        FileAlterationObserver o;
        if (!observer.containsKey(folder)) {
            o = new FileAlterationObserver(folder);
            observer.put(folder, o);
        } else {
            o = observer.get(folder);
        }

        if (!monitors.containsKey(folder)) {
            FileAlterationMonitor monitor = new FileAlterationMonitor(1000);
            monitor.addObserver(o);
            try {
                monitor.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        o.addListener(changeListener);

    }

}
