package de.obdev.thefink.jellyfish.util.socket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Jonas Born on 11.04.17.
 */
public class PackageInput implements Iterator<byte[]>, Runnable {


    byte[] last = new byte[0];
    Socket socket;
    CountDownLatch latch = new CountDownLatch(1);
    Long lastInput;
    private Boolean alive = true;

    public PackageInput(Socket socket) {
        this.socket = socket;
        Thread thread = new Thread(this);
        thread.start();


    }

    @Override
    public boolean hasNext() {
        return this.isAlive();
    }

    @Override
    public byte[] next() {
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return last;
    }

    public Long getLastInput() {
        return lastInput;
    }

    public Boolean isAlive() {
        return alive;
    }

    @Override
    public void run() {
        while (true) {
            Integer bytesRead;
            byte[] request = new byte[1024];
            Integer read = 0;
            Integer size = 0;
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            try {
                while (alive && (bytesRead = socket.getInputStream().read(request)) != -1) {
                    lastInput = System.currentTimeMillis();
                    if (size == 0) {
                        ByteBuffer buffer = ByteBuffer.wrap(request);
                        buffer.order(ByteOrder.LITTLE_ENDIAN);
                        size = buffer.getInt();
                    }
                    bout.write(request, 0, bytesRead);
                    read = read + bytesRead;
                    if (read >= size) {
                        last = bout.toByteArray();
                        bout.reset();
                        latch.countDown();
                        latch = new CountDownLatch(1);

                        size = 0;
                        read = 0;
                    }
                }
                alive = false;
            } catch (IOException e) {
                alive = false;
            }

        }
    }
}
