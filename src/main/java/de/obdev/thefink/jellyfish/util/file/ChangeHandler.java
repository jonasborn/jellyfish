package de.obdev.thefink.jellyfish.util.file;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;

import java.io.File;

/**
 * Created by Jonas Born on 23.04.17.
 */
public class ChangeHandler implements FileAlterationListener {

    ChangeListener listener;

    public ChangeHandler(ChangeListener listener) {
        this.listener = listener;
    }

    @Override
    public void onStart(FileAlterationObserver fileAlterationObserver) {
        listener.start();
    }

    @Override
    public void onDirectoryCreate(File file) {
        listener.directoryCreate(file);
    }

    @Override
    public void onDirectoryChange(File file) {
        listener.directoryChange(file);
    }

    @Override
    public void onDirectoryDelete(File file) {
        listener.directoryDelete(file);
    }

    @Override
    public void onFileCreate(File file) {
        listener.fileCreate(file);
    }

    @Override
    public void onFileChange(File file) {
        listener.fileChanged(file);
    }

    @Override
    public void onFileDelete(File file) {
        listener.fileDelete(file);
    }

    @Override
    public void onStop(FileAlterationObserver fileAlterationObserver) {
        listener.stop();
    }
}
