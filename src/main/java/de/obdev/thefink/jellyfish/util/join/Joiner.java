package de.obdev.thefink.jellyfish.util.join;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.zjsonpatch.JsonDiff;
import com.flipkart.zjsonpatch.JsonPatch;
import com.google.gson.Gson;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Iterator;

/**
 * Created by Jonas Born on 24.04.17.
 */
public class Joiner {

    static Gson gson = new Gson();

    public static <T> void join(T target, T input) {


        Class cls = target.getClass();
        for (Field field : cls.getDeclaredFields()) {
            field.setAccessible(true);
            try {
                Object result = field.get(input);
                if (result != null) {
                    field.set(target, result);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

    }


    public static <T> T add(T target, T input) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode n2 = mapper.readTree(gson.toJson(target));
        JsonNode n1 = mapper.readTree(gson.toJson(input));

        JsonNode patch = JsonDiff.asJson(
            n1,
            n2
        );

        clear(patch);

        return (T) gson.fromJson(
                JsonPatch.apply(patch, n1).toString(),
                target.getClass()
        );

    }

    public static void clear(JsonNode node) {
        Iterator<JsonNode> it = node.iterator();
        while (it.hasNext()) {
            JsonNode next = it.next();
            if (next.has("op")) {
                if (next.get("op").asText().equals("remove")) {
                    it.remove();
                }
            }
        }
    }

}
