package de.obdev.thefink.jellyfish.util.baler.converter;

/**
 * Created by Jonas Born on 07.05.17.
 */
public class BalerConverterException extends RuntimeException {

    public BalerConverterException() {
    }

    public BalerConverterException(String s) {
        super(s);
    }

    public BalerConverterException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public BalerConverterException(Throwable throwable) {
        super(throwable);
    }

    public BalerConverterException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
