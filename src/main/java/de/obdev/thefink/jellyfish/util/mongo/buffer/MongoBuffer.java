package de.obdev.thefink.jellyfish.util.mongo.buffer;


import de.obdev.thefink.jellyfish.util.mongo.document.MongoDocument;
import org.bson.BsonType;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jonas Born on 13.04.17.
 */
public class MongoBuffer {

    ByteBuffer buffer;

    public MongoBuffer(ByteBuffer buffer) {
        this.buffer = buffer;
    }

    public ByteBuffer asByteBuffer() {
        return buffer;
    }

    public String getString() {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        byte current;
        while ((current = this.get()) != 0) {
            bout.write(current);
        }
        byte[] data = bout.toByteArray();
        try {
            bout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String(data);
    }

    public BsonType getType() {
        return BsonType.findByValue(this.get());
    }

    public MongoBuffer slice() {
        buffer.slice();
        return this;
    }


    public MongoDocument getDocument() throws IOException {
        Integer length = this.getInt();
        byte[] data = asByteArray(length - 4);
        return new MongoDocument(ByteBuffer.wrap(data));
    }

    public List<MongoDocument> getDocuments() throws IOException {
        List<MongoDocument> list = new ArrayList<>();
        while (buffer.hasRemaining()) {
            list.add(new MongoDocument(this.getDocument()));
        }
        return list;

    }


    public List<Long> getLongs() {
        List<Long> list = new ArrayList<>();
        while (buffer.hasRemaining()) {
            list.add(this.getLong());
        }
        return list;
    }

    public byte[] asByteArray(Integer length) {
        Integer read = 0;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        byte[] buffer = new byte[64];
        while (read < length) {
            try {
                bout.write(buffer);
            } catch (IOException e) {
                e.printStackTrace();
            }
            read = read + buffer.length;
        }
        return bout.toByteArray();
    }

    public MongoBuffer duplicate() {
        buffer.duplicate();
        return this;
    }


    public MongoBuffer asReadOnlyBuffer() {
        buffer.asReadOnlyBuffer();
        return this;
    }


    public byte get() {
        return buffer.get();
    }


    public MongoBuffer put(byte b) {
        buffer.put(b);
        return this;
    }


    public byte get(int i) {
        return buffer.get(i);
    }


    public MongoBuffer put(int i, byte b) {
        buffer.put(i, b);
        return this;
    }


    public MongoBuffer compact() {
        buffer.compact();
        return this;
    }


    public boolean isReadOnly() {
        return buffer.isReadOnly();
    }


    public boolean isDirect() {
        return buffer.isDirect();
    }


    public char getChar() {
        return buffer.getChar();
    }


    public MongoBuffer putChar(char c) {
        buffer.putChar(c);
        return this;
    }


    public char getChar(int i) {
        return buffer.getChar(i);
    }


    public MongoBuffer putChar(int i, char c) {
        buffer.putChar(i, c);
        return this;
    }


    public CharBuffer asCharBuffer() {
        return buffer.asCharBuffer();
    }


    public short getShort() {
        return buffer.getShort();
    }


    public MongoBuffer putShort(short i) {
        buffer.putShort(i);
        return this;
    }


    public short getShort(int i) {
        return buffer.getShort();
    }


    public MongoBuffer putShort(int i, short i1) {
        buffer.putShort(i, i1);
        return this;
    }


    public ShortBuffer asShortBuffer() {
        return buffer.asShortBuffer();
    }


    public int getInt() {
        return buffer.getInt();
    }


    public MongoBuffer putInt(int i) {
        buffer.putInt(i);
        return this;
    }


    public int getInt(int i) {
        return buffer.getInt(i);
    }


    public MongoBuffer putInt(int i, int i1) {
        buffer.putInt(i, i1);
        return this;
    }


    public IntBuffer asIntBuffer() {
        return buffer.asIntBuffer();
    }


    public long getLong() {
        return buffer.getLong();
    }


    public MongoBuffer putLong(long l) {
        this.putLong(l);
        return this;
    }


    public long getLong(int i) {
        return buffer.getLong(i);
    }


    public MongoBuffer putLong(int i, long l) {
        buffer.putLong(i, l);
        return this;
    }


    public LongBuffer asLongBuffer() {
        return buffer.asLongBuffer();
    }


    public float getFloat() {
        return this.getFloat();
    }


    public MongoBuffer putFloat(float v) {
        this.putFloat(v);
        return this;
    }


    public float getFloat(int i) {
        return buffer.getFloat(i);
    }


    public MongoBuffer putFloat(int i, float v) {
        buffer.putFloat(i, v);
        return this;
    }


    public FloatBuffer asFloatBuffer() {
        return buffer.asFloatBuffer();
    }


    public double getDouble() {
        return buffer.getDouble();
    }


    public MongoBuffer putDouble(double v) {
        buffer.putDouble(v);
        return this;
    }


    public double getDouble(int i) {
        return buffer.getDouble(i);
    }


    public MongoBuffer putDouble(int i, double v) {
        buffer.putDouble(i, v);
        return this;
    }


    public DoubleBuffer asDoubleBuffer() {
        return buffer.asDoubleBuffer();
    }
}