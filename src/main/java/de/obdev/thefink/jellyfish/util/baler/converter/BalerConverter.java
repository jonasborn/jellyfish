package de.obdev.thefink.jellyfish.util.baler.converter;

import com.github.wnameless.json.flattener.JsonFlattener;
import com.github.wnameless.json.unflattener.JsonUnflattener;
import org.xerial.snappy.Snappy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by Jonas Born on 07.05.17.
 */
public class BalerConverter {

    static JsonCompressor compressor = new JsonCompressor();

    public static byte[] compress(String json) throws IOException {
        if (json.length() < 150) {
            byte[] compressed = compressor.compressJson(json);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream(compressed.length + 1);
            outputStream.write(0x00);
            outputStream.write(compressed);
            byte[] finalCompressed = outputStream.toByteArray();
            outputStream.close();
            return finalCompressed;
        } else {
            byte[] compressed = bigCompress(json);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream(compressed.length + 1);
            outputStream.write(0x01);
            outputStream.write(compressed);
            byte[] finalCompressed = outputStream.toByteArray();
            outputStream.close();
            return finalCompressed;
        }
    }

    public static String decompress(byte[] data) throws IOException {

        ByteBuffer buffer = ByteBuffer.wrap(data);

        Byte marker = buffer.get();
        if (marker == 0x00) {
            byte[] finalData = new byte[buffer.remaining()];
            buffer.get(finalData);
            buffer.clear();
            return compressor.expandJson(finalData);
        } else if (marker == 0x01) {
            byte[] finalData = new byte[buffer.remaining()];
            buffer.get(finalData);
            buffer.clear();
            return bigDecompress(finalData);
        }
        throw new BalerConverterException("Unable to decompress data marked with " + marker);
    }

    public static byte[] bigCompress(String data) throws IOException {
        String flatten = JsonFlattener.flatten(data);
        return Snappy.compress(flatten);
    }

    public static String bigDecompress(byte[] compressed) throws IOException {
        String json = new String(Snappy.uncompress(compressed), "UTF-8");
        return JsonUnflattener.unflatten(json);
    }

}
