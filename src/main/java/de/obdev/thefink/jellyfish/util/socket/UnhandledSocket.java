package de.obdev.thefink.jellyfish.util.socket;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Proxy;
import java.net.Socket;

/**
 * Created by Jonas Born on 26.04.17.
 */
public class UnhandledSocket extends AbstractSocket {

    public UnhandledSocket() {
    }

    public UnhandledSocket(Proxy proxy) {
        super(proxy);
    }

    public UnhandledSocket(Socket socket) {
        super(socket);
    }

    public UnhandledSocket(String s, int i) throws IOException {
        super(s, i);
    }

    public UnhandledSocket(InetAddress inetAddress, int i) throws IOException {
        super(inetAddress, i);
    }

    public UnhandledSocket(String s, int i, InetAddress inetAddress, int i1) throws IOException {
        super(s, i, inetAddress, i1);
    }

    public UnhandledSocket(InetAddress inetAddress, int i, InetAddress inetAddress1, int i1) throws IOException {
        super(inetAddress, i, inetAddress1, i1);
    }
}
