package de.obdev.thefink.jellyfish.util.socket;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 * Created by Jonas Born on 12.04.17.
 */
public class PackageOutput {

    Socket socket;

    public PackageOutput(Socket socket) {
        this.socket = socket;
    }

    public void write(byte[] bytes) throws IOException {
        write(new ByteArrayInputStream(bytes));
    }

    public void write(InputStream stream) throws IOException {
        Integer read = 0;
        Integer current;
        byte[] buffer = new byte[1024];
        while ((current = stream.read(buffer)) != -1) {
            socket.getOutputStream().write(buffer, 0, current);
            read = read + current;
        }
        socket.getOutputStream().flush();
    }


}
