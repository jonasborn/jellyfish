package de.obdev.thefink.jellyfish.util.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;
import java.nio.channels.SocketChannel;

/**
 * Created by Jonas Born on 26.04.17.
 */
public class AbstractSocket extends Socket {

    Socket socket;

    public AbstractSocket() {
        socket = new Socket();
    }

    public AbstractSocket(Proxy proxy) {
        socket = new Socket(proxy);
    }

    public AbstractSocket(Socket socket) {
        this.socket = socket;
    }

    public AbstractSocket(String s, int i) throws IOException {
        socket = new Socket(s, i);
    }

    public AbstractSocket(InetAddress inetAddress, int i) throws IOException {
        socket = new Socket(inetAddress, i);
    }

    public AbstractSocket(String s, int i, InetAddress inetAddress, int i1) throws IOException {
        socket = new Socket(s, i, inetAddress, i1);
    }

    public AbstractSocket(InetAddress inetAddress, int i, InetAddress inetAddress1, int i1) throws IOException {
        socket = new Socket(inetAddress, i, inetAddress1, i1);
    }

    @Override
    public void connect(SocketAddress socketAddress) throws IOException {
        socket.connect(socketAddress);
    }

    @Override
    public void connect(SocketAddress socketAddress, int i) throws IOException {
        socket.connect(socketAddress, i);
    }

    @Override
    public void bind(SocketAddress socketAddress) throws IOException {
        socket.bind(socketAddress);
    }

    @Override
    public InetAddress getInetAddress() {
        return socket.getInetAddress();
    }

    @Override
    public InetAddress getLocalAddress() {
        return socket.getLocalAddress();
    }

    @Override
    public int getPort() {
        return socket.getPort();
    }

    @Override
    public int getLocalPort() {
        return socket.getLocalPort();
    }

    @Override
    public SocketAddress getRemoteSocketAddress() {
        return socket.getRemoteSocketAddress();
    }

    @Override
    public SocketAddress getLocalSocketAddress() {
        return socket.getLocalSocketAddress();
    }

    @Override
    public SocketChannel getChannel() {
        return socket.getChannel();
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        return socket.getOutputStream();
    }

    @Override
    public void setTcpNoDelay(boolean b) throws SocketException {
        socket.setTcpNoDelay(b);
    }

    @Override
    public boolean getTcpNoDelay() throws SocketException {
        return socket.getTcpNoDelay();
    }

    @Override
    public void setSoLinger(boolean b, int i) throws SocketException {
        socket.setSoLinger(b, i);
    }

    @Override
    public int getSoLinger() throws SocketException {
        return socket.getSoLinger();
    }

    @Override
    public void sendUrgentData(int i) throws IOException {
        socket.sendUrgentData(i);
    }

    @Override
    public void setOOBInline(boolean b) throws SocketException {
        socket.setOOBInline(b);
    }

    @Override
    public boolean getOOBInline() throws SocketException {
        return socket.getOOBInline();
    }

    @Override
    public synchronized void setSoTimeout(int i) throws SocketException {
        socket.setSoTimeout(i);
    }

    @Override
    public synchronized int getSoTimeout() throws SocketException {
        return socket.getSoTimeout();
    }

    @Override
    public synchronized void setSendBufferSize(int i) throws SocketException {
        socket.setSendBufferSize(i);
    }

    @Override
    public synchronized int getSendBufferSize() throws SocketException {
        return socket.getSendBufferSize();
    }

    @Override
    public synchronized void setReceiveBufferSize(int i) throws SocketException {
        socket.setReceiveBufferSize(i);
    }

    @Override
    public synchronized int getReceiveBufferSize() throws SocketException {
        return socket.getReceiveBufferSize();
    }

    @Override
    public void setKeepAlive(boolean b) throws SocketException {
        socket.setKeepAlive(b);
    }

    @Override
    public boolean getKeepAlive() throws SocketException {
        return socket.getKeepAlive();
    }

    @Override
    public void setTrafficClass(int i) throws SocketException {
        socket.setTrafficClass(i);
    }

    @Override
    public int getTrafficClass() throws SocketException {
        return socket.getTrafficClass();
    }

    @Override
    public void setReuseAddress(boolean b) throws SocketException {
        socket.setReuseAddress(b);
    }

    @Override
    public boolean getReuseAddress() throws SocketException {
        return socket.getReuseAddress();
    }

    @Override
    public synchronized void close() throws IOException {
        socket.close();
    }

    @Override
    public void shutdownInput() throws IOException {
        socket.shutdownInput();
    }

    @Override
    public void shutdownOutput() throws IOException {
        socket.shutdownOutput();
    }

    @Override
    public String toString() {
        return socket.toString();
    }

    @Override
    public boolean isConnected() {
        return socket.isConnected();
    }

    @Override
    public boolean isBound() {
        return socket.isBound();
    }

    @Override
    public boolean isClosed() {
        return socket.isClosed();
    }

    @Override
    public boolean isInputShutdown() {
        return socket.isInputShutdown();
    }

    @Override
    public boolean isOutputShutdown() {
        return socket.isOutputShutdown();
    }

    @Override
    public void setPerformancePreferences(int i, int i1, int i2) {
        socket.setPerformancePreferences(i, i1, i2);
    }

}
