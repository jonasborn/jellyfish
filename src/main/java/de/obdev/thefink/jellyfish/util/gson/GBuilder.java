package de.obdev.thefink.jellyfish.util.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.obdev.thefink.jellyfish.config.JellyfishId;
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage;
import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import io.github.lukehutch.fastclasspathscanner.matchprocessor.SubclassMatchProcessor;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Jonas Born on 07.05.17.
 */
public class GBuilder {

    static Gson gson;

    static {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Reflections reflections = new Reflections("de.obdev", new SubTypesScanner(false));
        Set<Class<? extends Object>> classes = reflections.getSubTypesOf(Object.class);

        for (Class<?> cls:classes) {
            if (Modifier.isAbstract(cls.getModifiers())) {
                gsonBuilder.registerTypeAdapter(cls, new InterfaceAdapter());
            }
        }
        gsonBuilder.registerTypeAdapter(JellyfishId.class, new JellyfishIdAdapter());
        gsonBuilder.registerTypeAdapter(byte[].class, new ByteArrayAdapter());
        gson = gsonBuilder.create();
    }

    public static Gson get() {
        return gson;
    }

}
