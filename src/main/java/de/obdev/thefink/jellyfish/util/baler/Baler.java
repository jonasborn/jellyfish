package de.obdev.thefink.jellyfish.util.baler;

import com.google.gson.*;
import de.obdev.thefink.jellyfish.util.baler.converter.BalerConverter;
import de.obdev.thefink.jellyfish.util.baler.converter.JsonCompressor;
import de.obdev.thefink.jellyfish.util.gson.GBuilder;

import java.io.*;

/**
 * Created by Jonas Born on 06.05.17.
 */
public class Baler {

    static Gson gson = GBuilder.get();
    static JsonParser parser = new JsonParser();

    public static byte[] pack(Object o) throws IOException {

        JsonElement element = gson.toJsonTree(o);
        JsonObject object = new JsonObject();
        if (element instanceof JsonPrimitive) {
            object.add(".+", element);
        } else {
            object = element.getAsJsonObject();
        }

        object.addProperty(".#", o.getClass().getName());
        System.out.println(gson.toJson(object));
        return BalerConverter.compress(
                object.toString()
        );
    }

    public static Object unpack(byte[] b) throws IOException, ClassNotFoundException {
        JsonObject object = (JsonObject) parser.parse(
                BalerConverter.decompress(b)
        );
        String clsName = object.get(".#").getAsString();
        Class cls = Class.forName(clsName);

        if (object.has(".+")) {
            return gson.fromJson(object.get(".+"), cls);
        } else {
            return gson.fromJson(object, cls);
        }

    }




}
