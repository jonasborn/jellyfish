package de.obdev.thefink.jellyfish.util.file;

import java.io.File;

/**
 * Created by Jonas Born on 16.04.17.
 */
public abstract class ChangeListener {

    public void start() {
    }

    public void directoryCreate(File file) {
    }


    public void directoryChange(File file) {
    }


    public void directoryDelete(File file) {
    }


    public void fileCreate(File file) {
    }


    public void fileChanged(File file) {
    }


    public void fileDelete(File file) {
    }

    public void stop() {
    }


}
