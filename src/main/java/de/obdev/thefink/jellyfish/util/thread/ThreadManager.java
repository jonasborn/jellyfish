package de.obdev.thefink.jellyfish.util.thread;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Callable;

/**
 * Created by Jonas Born on 10.05.17.
 */
public class ThreadManager {

    static Boolean running = true;
    static Logger logger = LogManager.getLogger();

    public static String name() {
        logger.warn("Hallo Welt");
        StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
        StackTraceElement e = stacktrace[3];//maybe this number needs to be corrected
        String name = e.getClassName() + "." + e.getMethodName() + "(" + e.getLineNumber() + ")";
        return name;
    }

    public static void shutdown() {
        running = false;
    }

    public static Boolean keepRunning() {
        return running;
    }

    public static Thread getThread(Runnable callable) {
        String name = name();
        Thread thread = new Thread(callable);
        thread.setName(name);
        return thread;
    }
}
