package de.obdev.thefink.jellyfish.util.gson;

import com.google.gson.*;
import org.apache.commons.codec.binary.Base64;

import java.lang.reflect.Type;

/**
 * Created by Jonas Born on 10.05.17.
 */
public class ByteArrayAdapter implements JsonSerializer<byte[]>, JsonDeserializer<byte[]> {
    @Override
    public JsonElement serialize(byte[] bytes, Type type, JsonSerializationContext jsonSerializationContext) {
        String base = Base64.encodeBase64URLSafeString(bytes);
        return new JsonPrimitive(base);
    }

    @Override
    public byte[] deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return Base64.decodeBase64(jsonElement.getAsString());
    }
}
