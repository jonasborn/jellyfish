package de.obdev.thefink.jellyfish.util.mongo.document;

import com.mongodb.MongoClient;
import com.mongodb.connection.ByteBufferBsonOutput;
import com.mongodb.internal.connection.PowerOfTwoBufferPool;
import org.bson.*;
import org.bson.codecs.BsonDocumentCodec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import org.bson.codecs.configuration.CodecRegistries;

import java.nio.ByteBuffer;
import java.util.Map;

/**
 * Created by Jonas Born on 18.04.17.
 */
public class MongoDocument extends Document {

    public MongoDocument() {
    }

    public MongoDocument(String key, Object value) {
        super(key, value);
    }

    public MongoDocument(Map<String, Object> map) {
        super(map);
    }

    public MongoDocument(ByteBuffer buffer) {
        BsonReader reader = new BsonBinaryReader(buffer);
        BsonDocumentCodec documentCodec = new BsonDocumentCodec();
        BsonDocument document = documentCodec.decode(reader, DecoderContext.builder().build());
        this.putAll(document);
    }

    public byte[] toByteArray() {
        BsonDocumentCodec documentCodec = new BsonDocumentCodec();
        ByteBufferBsonOutput output = new ByteBufferBsonOutput(
                new PowerOfTwoBufferPool()
        );
        BsonWriter writer = new BsonBinaryWriter(output);

        BsonDocument document = this.toBsonDocument(BsonDocument.class, CodecRegistries.fromCodecs(MongoClient.getDefaultCodecRegistry().get(Document.class)));

        documentCodec.encode(writer, document, EncoderContext.builder().build());
        return output.toByteArray();
    }
}
