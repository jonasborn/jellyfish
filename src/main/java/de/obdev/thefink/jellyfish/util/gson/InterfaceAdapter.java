package de.obdev.thefink.jellyfish.util.gson;

/**
 * Created by Jonas Born on 07.05.17.
 */
import com.google.gson.*;

import java.lang.reflect.Type;

public class InterfaceAdapter
        implements JsonSerializer, JsonDeserializer {


    @Override
    public final JsonElement serialize(final Object object, final Type interfaceType, final JsonSerializationContext context) {
        final JsonObject member = new JsonObject();
        member.addProperty(".#", object.getClass().getName());
        member.add(".", context.serialize(object));
        return member;
    }

    @Override
    public final Object deserialize(final JsonElement elem, final Type interfaceType, final JsonDeserializationContext context) throws JsonParseException {
        final JsonObject member = (JsonObject) elem;
        final JsonElement typeString = get(member, ".#");
        final JsonElement data = get(member, ".");
        final Type actualType = typeForName(typeString);
        return context.deserialize(data, actualType);
    }

    private Type typeForName(final JsonElement typeElem) {
        try {
            return Class.forName(typeElem.getAsString());
        }
        catch (ClassNotFoundException e) {
            throw new JsonParseException(e);
        }
    }

    private JsonElement get(final JsonObject wrapper, final String memberName) {
        final JsonElement elem = wrapper.get(memberName);
        if (elem == null) {
            throw new JsonParseException("no '" + memberName + "' member found in json file.");
        }
        return elem;
    }

}