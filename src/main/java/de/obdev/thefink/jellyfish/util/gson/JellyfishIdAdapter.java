package de.obdev.thefink.jellyfish.util.gson;

import java.io.IOException;
import java.lang.reflect.Type;


import com.google.gson.*;
import de.obdev.thefink.jellyfish.config.JellyfishId;

public class JellyfishIdAdapter implements JsonSerializer<JellyfishId>, JsonDeserializer<JellyfishId> {

    @Override
    public JsonElement serialize(JellyfishId jellyfishId, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonParser().parse(jellyfishId.toString());
    }

    @Override
    public JellyfishId deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        try {
            return JellyfishId.fromString(jsonElement.getAsString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}