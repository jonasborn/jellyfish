package de.obdev.thefink.jellyfish.util.socket;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Proxy;
import java.net.Socket;

/**
 * Created by Jonas Born on 26.04.17.
 */
public class HandledSocket extends AbstractSocket {


    public HandledSocket() {
    }

    public HandledSocket(Proxy proxy) {
        super(proxy);
    }

    public HandledSocket(Socket socket) {
        super(socket);
    }

    public HandledSocket(String s, int i) throws IOException {
        super(s, i);
    }

    public HandledSocket(InetAddress inetAddress, int i) throws IOException {
        super(inetAddress, i);
    }

    public HandledSocket(String s, int i, InetAddress inetAddress, int i1) throws IOException {
        super(s, i, inetAddress, i1);
    }

    public HandledSocket(InetAddress inetAddress, int i, InetAddress inetAddress1, int i1) throws IOException {
        super(inetAddress, i, inetAddress1, i1);
    }
}
