package de.obdev.thefink.jellyfish.runner;

/**
 * Created by Jonas Born on 25.04.17.
 */
public abstract class Runner {

    Boolean running = true;

    public Boolean isRunning() {
        return running;
    }

    public Runner setRunning(Boolean running) {
        this.running = running;
        return this;
    }

    public abstract void start();

    public abstract void run();

    public abstract void stop();

}
