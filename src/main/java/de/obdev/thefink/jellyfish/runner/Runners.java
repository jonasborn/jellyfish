package de.obdev.thefink.jellyfish.runner;


import de.obdev.thefink.jellyfish.action.Startable;
import de.obdev.thefink.jellyfish.scripts.ScriptListener;
import de.obdev.thefink.jellyfish.scripts.Scripts;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by Jonas Born on 25.04.17.
 */
public class Runners {

    static Logger logger = LogManager.getLogger();

    static List<Runner> runners = new ArrayList<>();

    static ExecutorService service = Executors.newCachedThreadPool();

    static Map<Runner, Future> futures = new HashMap<>();

    static {
        Scripts.addListener(
                new ScriptListener() {
                    @Override
                    public void onLoad(Class cls) {
                        add(cls);
                    }

                    @Override
                    public void onRemove(Class cls) {
                        remove(cls);
                    }
                }
        );
    }

    @Startable
    public static void start() {

    }

    static void add(Class cls) {
        if (Runner.class.isAssignableFrom(cls)) {
            try {
                Runner runner = (Runner) cls.newInstance();
                logger.info("Added runner ({})", cls.getName());
                runners.add(runner);
                runner.start();
                futures.put(runner, service.submit(() -> runner.run()));
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    static void remove(Class cls) {
        if (Runner.class.isAssignableFrom(cls)) {
            runners.removeIf((e) ->
                    {
                        if (e.getClass().getName().equals(cls.getName())) {
                            logger.info("Removed runner ({})", e.getClass().getName());
                            Future future = futures.get(e);
                            e.setRunning(false);
                            e.stop();
                            future.cancel(true);
                            futures.remove(e);
                            return true;
                        } else {
                            return false;
                        }
                    }
            );
        }
    }

}
