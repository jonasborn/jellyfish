package de.obdev.thefink.jellyfish;

import de.obdev.thefink.jellyfish.action.Actions;
import de.obdev.thefink.jellyfish.config.Configs;
import de.obdev.thefink.jellyfish.config.JellyfishId;
import de.obdev.thefink.jellyfish.connection.AbstractConnection;
import de.obdev.thefink.jellyfish.connection.Connections;
import de.obdev.thefink.jellyfish.connection.JellyfishConnection;
import de.obdev.thefink.jellyfish.message.jellyfish.status.BasicStatusMessage;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by Jonas Born on 01.05.17.
 */
public class Jelly1 {

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException, IOException, InterruptedException, ClassNotFoundException {


        Configurator.setRootLevel(Level.INFO);

        Configs.setFile(new File("jelly1.json"));
        Configs.read();


        Actions.start();

        Thread.sleep(4000);

        for (AbstractConnection connection:Connections.get(JellyfishConnection.class)) {

            if (connection instanceof JellyfishConnection) {

                connection.write(
                        new BasicStatusMessage(
                                ((JellyfishConnection) connection).getOwner()
                        )
                );
            }

        }

    }

}
