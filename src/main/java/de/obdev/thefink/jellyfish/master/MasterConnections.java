package de.obdev.thefink.jellyfish.master;


import de.obdev.thefink.jellyfish.action.Startable;
import de.obdev.thefink.jellyfish.config.Configs;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Jonas Born on 19.04.17.
 */
public class MasterConnections implements Runnable {

    Logger logger = LogManager.getLogger();

    static List<MasterConnection> connections = new ArrayList<>();

    static String host = "149.202.162.119";
    static Integer port = 27017;

    static {
        new Thread(new MasterConnections()).start();
    }

    @Startable
    public static void start() {

    }

    public static MasterConnection get() {
        Iterator<MasterConnection> it = connections.iterator();
        while (it.hasNext()) {
            MasterConnection connection = it.next();
            if (connection.isUsable()) {
                return connection;
            }
        }
        try {
            MasterConnection connection = new MasterConnection(new Socket(host, port));
            connections.add(connection);
            return connection;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void run() {
        List<MasterConnection> connections = new ArrayList<>(MasterConnections.connections);
        Integer size = connections.size();
        while (size < 3) {
            try {
                connections.add(
                        new MasterConnection(
                                new Socket(host, port)
                        )
                );
                logger.info("Created new connection");
            } catch (IOException e) {
                e.printStackTrace();
            }
            size++;
        }

        host = Configs.current().getMongoConfig().getAddress().getHost();
        port = Configs.current().getMongoConfig().getAddress().getPort();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
