package de.obdev.thefink.jellyfish.master;


import de.obdev.thefink.jellyfish.util.socket.PackageInput;
import de.obdev.thefink.jellyfish.util.socket.PackageOutput;

import java.io.IOException;
import java.net.Socket;

/**
 * Created by Jonas Born on 19.04.17.
 */
public class MasterConnection {

    Socket socket;

    PackageInput input;
    PackageOutput output;

    Boolean usable = false;

    public MasterConnection(Socket socket) {
        this.socket = socket;
        input = new PackageInput(socket);
        output = new PackageOutput(socket);
    }

    public MasterConnection write(byte[] bytes) throws IOException {
        usable = false;
        output.write(bytes);
        return this;
    }

    public byte[] read() {
        byte[] result = input.next();
        usable = true;
        return result;
    }

    public void block() {
        usable = false;
    }

    public Boolean isUsable() {
        return usable;
    }
}
