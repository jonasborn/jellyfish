package de.obdev.thefink.jellyfish.connection;

import com.google.gson.Gson;
import de.obdev.thefink.jellyfish.util.thread.ThreadManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jonas Born on 28.04.17.
 */
public class Connections {

    static Logger logger = LogManager.getLogger();

    private static List<AbstractConnection> connections = new ArrayList<>();

    static {
        ThreadManager.getThread(() -> {
            while (ThreadManager.keepRunning()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                connections.removeIf((e) -> {
                    Boolean remove = !e.isAlive();
                    if (remove) {
                        logger.info("Removed unused connection ({})", e.debugId());
                    }
                    return remove;
                });
            }
        }).start();
    }

    private Connections() {

    }

    public static Integer count(Class... classes) {
        Integer count = 0;
        for (AbstractConnection connection : connections) {

            Boolean match = connection.isAlive();
            for (Class cls : classes) {
                if (!cls.isAssignableFrom(connection.getClass())) {
                    match = false;
                }
            }
            if (match) count++;
        }
        return count;
    }

    public static void notifyStartup(AbstractConnection connection) {
        if (!connections.contains(connection)) {
            connections.add(connection);
        }
    }

    public static List<AbstractConnection> get(Class... classes) {
        List<AbstractConnection> results = new ArrayList<>();
        for (AbstractConnection connection : connections) {
            Boolean match = connection.isAlive();
            for (Class cls : classes) {
                if (!cls.isAssignableFrom(connection.getClass())) {
                    match = false;
                }
            }
            if (match) results.add(connection);
        }
        return results;
    }



    /*
    public static AbstractConnection get(ConnectionFilter filter) {
        for (AbstractConnection connection:connections) {
            if (filter.match(convert(connection))) return connection;
        }
        return null;
    }

    public static Map<Object, Object> convert(Object o) {
        Type type = new TypeToken<Map<Object, Object>>(){}.getType();
        return gson.fromJson(gson.toJson(o), type);
    }

    public interface ConnectionFilter {
        public boolean match(Map<Object, Object> map);
    }
    */
}
