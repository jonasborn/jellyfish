package de.obdev.thefink.jellyfish.connection;

import de.obdev.thefink.jellyfish.message.mongo.MongoMessage;

import java.net.Socket;

/**
 * Created by Jonas Born on 26.04.17.
 */
public class MongoConnection extends AbstractConnection<MongoMessage> {
    public MongoConnection(Socket socket) {
        super(socket);
    }
}
