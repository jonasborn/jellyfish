package de.obdev.thefink.jellyfish.connection;

import de.obdev.thefink.jellyfish.decoder.MessageDecoders;
import de.obdev.thefink.jellyfish.handler.Handlers;
import de.obdev.thefink.jellyfish.message.AbstractMessage;
import de.obdev.thefink.jellyfish.util.socket.PackageInput;
import de.obdev.thefink.jellyfish.util.socket.PackageOutput;
import org.apache.commons.codec.binary.Hex;

import java.io.IOException;
import java.net.Socket;

/**
 * Created by Jonas Born on 26.04.17.
 */
public class AbstractConnection<T extends AbstractMessage> {

    Socket socket;
    PackageOutput output;
    Boolean alive = true;

    public AbstractConnection(Socket socket) {
        setSocket(socket);
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
        output = new PackageOutput(socket);
        Connections.notifyStartup(this);
    }

    public AbstractConnection start() {
        new Thread(() -> {
            PackageInput pi = new PackageInput(socket);
            while (pi.hasNext()) {
                byte[] input = pi.next();
                try {
                    AbstractMessage message = MessageDecoders.decode(input.clone());
                    System.out.println("Received Conn: " + message.getClass().getName());
                    byte[] result = Handlers.handle(input, message, this);
                    try {
                        output.write(result);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
            alive = false;
        }).start();
        return this;
    }

    public void write(byte[] bytes) {
        System.out.println("Written via " + this.hashCode());
        try {
            output.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(T message) {
        System.out.println("Written via " + this.hashCode());
        try {
            write(message.toByteArray());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Boolean isAlive() {
        return alive;
    }

    public String debugId() {
        return this.getClass().getName() + ":" + this.hashCode();
    }
}
