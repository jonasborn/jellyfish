package de.obdev.thefink.jellyfish.connection;

import de.obdev.thefink.jellyfish.config.JellyfishId;
import de.obdev.thefink.jellyfish.message.jellyfish.JellyfishMessage;

import java.net.Socket;
import java.util.UUID;

/**
 * Created by Jonas Born on 27.04.17.
 */
public class JellyfishConnection extends AbstractConnection<JellyfishMessage> {

    JellyfishId owner;

    public JellyfishConnection(Socket socket) {
        super(socket);
    }

    public JellyfishId getOwner() {
        return owner;
    }

    public JellyfishConnection setOwner(JellyfishId owner) {
        this.owner = owner;
        return this;
    }
}
