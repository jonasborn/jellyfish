package de.obdev.thefink.jellyfish.server;

import com.google.common.reflect.TypeToken;
import de.obdev.thefink.jellyfish.connection.AbstractConnection;
import de.obdev.thefink.jellyfish.util.socket.HandledSocket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Jonas Born on 26.04.17.
 */
public class AbstractServer<T extends AbstractConnection> {

    private final TypeToken<T> typeToken = new TypeToken<T>(getClass()) {
    };
    private final Type type = typeToken.getType(); // or getRawType() to return Class<? super T>
    Logger logger = LogManager.getLogger();
    T connection;

    ServerSocket serverSocket;

    public AbstractServer(Integer port) {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info(
                "Started server ({}) on port {}",
                this.getClass(),
                port
        );
    }

    public AbstractServer start() {
        new Thread(() -> {
            while (true) {
                try {
                    Socket socket = new HandledSocket(serverSocket.accept());
                    logger.info("Accepted connection ({}) from {}", type, socket.getLocalAddress());
                    try {
                        Class cls = Class.forName(typeToken.getType().getTypeName());
                        connection = (T) cls.getDeclaredConstructor(Socket.class).newInstance(socket);
                        connection.start();
                    } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        return this;
    }

}
