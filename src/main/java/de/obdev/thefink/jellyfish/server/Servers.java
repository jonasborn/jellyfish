package de.obdev.thefink.jellyfish.server;

import de.obdev.thefink.jellyfish.action.Startable;
import de.obdev.thefink.jellyfish.encoder.AbstractMessageEncoder;
import org.apache.commons.logging.Log;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Jonas Born on 26.04.17.
 */
public class Servers {

    static Logger logger = LogManager.getLogger();

    static List<AbstractServer> servers = new ArrayList<>();

    static {
        Reflections reflections = new Reflections();
        Set<Class<? extends AbstractServer>> encs =  reflections.getSubTypesOf(AbstractServer.class);

        for (Class<? extends AbstractServer> enc:encs) {
            if (!Modifier.isAbstract(enc.getModifiers())) {
                try {
                    AbstractServer e = enc.newInstance();
                    servers.add(e);
                    logger.info("Added server ({})", e.getClass());
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Startable
    public static void start() {
        for (AbstractServer server:servers) {
            server.start();
        }
    }

}
