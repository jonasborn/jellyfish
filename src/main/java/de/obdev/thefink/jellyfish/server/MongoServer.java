package de.obdev.thefink.jellyfish.server;

import de.obdev.thefink.jellyfish.config.Configs;
import de.obdev.thefink.jellyfish.connection.MongoConnection;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Jonas Born on 26.04.17.
 */
public class MongoServer extends AbstractServer<MongoConnection> {
    public MongoServer() {
        super(Configs.current().getMongoConfig().getPort());
    }
}
