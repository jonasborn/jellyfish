package de.obdev.thefink.jellyfish.server;

import de.obdev.thefink.jellyfish.config.Configs;
import de.obdev.thefink.jellyfish.connection.JellyfishConnection;
import de.obdev.thefink.jellyfish.server.AbstractServer;

/**
 * Created by Jonas Born on 27.04.17.
 */
public class JellyfishServer extends AbstractServer<JellyfishConnection> {
    public JellyfishServer() {
        super(Configs.current().getJellyfishConfig().getPort());
    }
}
