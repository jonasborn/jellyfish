package de.obdev.thefink.jellyfish.handler;


import de.obdev.thefink.jellyfish.connection.AbstractConnection;
import de.obdev.thefink.jellyfish.message.AbstractMessage;

/**
 * Created by Jonas Born on 22.04.17.
 */
public class HandlerInput<T extends AbstractMessage> {


    byte[] bytes;
    T message;
    AbstractConnection connection;

    public HandlerInput(byte[] bytes, T message, AbstractConnection connection) {
        this.bytes = bytes;
        this.message = message;
        this.connection = connection;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public T getMessage() {
        return message;
    }

    public AbstractConnection getConnection() {
        return connection;
    }
}
