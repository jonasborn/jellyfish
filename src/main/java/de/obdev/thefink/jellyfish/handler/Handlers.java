package de.obdev.thefink.jellyfish.handler;


import de.obdev.thefink.jellyfish.connection.AbstractConnection;
import de.obdev.thefink.jellyfish.message.AbstractMessage;
import de.obdev.thefink.jellyfish.scripts.ScriptListener;
import de.obdev.thefink.jellyfish.scripts.Scripts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Jonas Born on 20.04.17.
 */
public class Handlers {

    static Logger logger = LogManager.getLogger();

    static ExecutorService service = Executors.newCachedThreadPool();

    static List<SyncHandler> syncs = (new ArrayList<>());
    static List<AsyncHandler> asyncs = (new ArrayList<>());

    static {

        Scripts.addListener(new ScriptListener() {
            @Override
            public void onLoad(Class cls) {
                addHandler(cls);
            }

            @Override
            public void onRemove(Class cls) {
                removeHandler(cls);
            }
        });
    }

    public static void addHandler(Class cls) {

        if (SyncHandler.class.isAssignableFrom(cls)) {
            try {
                SyncHandler handler = (SyncHandler) cls.newInstance();
                logger.info("Added handler ({})", cls.getName());
                syncs.add(handler);
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (AsyncHandler.class.isAssignableFrom(cls)) {
            try {
                AsyncHandler handler = (AsyncHandler) cls.newInstance();
                logger.info("Added handler ({})", cls.getName());
                asyncs.add(handler);
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private static void removeHandler(Class cls) {

        if (SyncHandler.class.isAssignableFrom(cls)) {
            syncs.removeIf((e) ->
                    {
                        if (e.getClass().getName().equals(cls.getName())) {
                            logger.info("Removed handler ({})", e.getClass().getName());
                            return true;
                        } else {
                            return false;
                        }
                    }
            );
        } else if (AsyncHandler.class.isAssignableFrom(cls)) {
            asyncs.removeIf((e) ->
                    {
                        if (e.getClass().getName().equals(cls.getName())) {
                            logger.info("Removed handler ({})", e.getClass().getName());
                            return true;
                        } else {
                            return false;
                        }
                    }
            );
        }

    }


    public static void start() {

    }

    public static byte[] handle(byte[] bytes, AbstractMessage message, AbstractConnection connection) {
        List<AsyncHandler> asyncs = new ArrayList<>(Handlers.asyncs);
        for (AsyncHandler handler : asyncs) {
            try {
                Class cls = Class.forName(handler.getType().getTypeName());
                if (cls.isAssignableFrom(message.getClass())) {
                    service.submit(() -> {
                        handler.onRequest(
                                new HandlerInput(
                                        bytes,
                                        message,
                                        connection
                                )
                        );
                    });
                }
            } catch (ClassNotFoundException ignored) {

            }
        }

        List<SyncHandler> syncs = new ArrayList<>(Handlers.syncs);

        for (SyncHandler handler : syncs) {
            try {
                Class cls = Class.forName(handler.getType().getTypeName());

                if (cls.isAssignableFrom(message.getClass())) {
                    HandlerOutput output = handler.onRequest(
                            new HandlerInput(
                                    bytes,
                                    message,
                                    connection
                            )
                    );
                    if (output.getBytes() != null) {
                        return output.getBytes();
                    }
                }
            } catch (ClassNotFoundException ignored) {

            }
        }

        return new byte[0];
    }


}
