package de.obdev.thefink.jellyfish.handler;

import com.google.common.reflect.TypeToken;
import de.obdev.thefink.jellyfish.message.AbstractMessage;

import java.lang.reflect.Type;

/**
 * Created by Jonas Born on 20.04.17.
 */

public abstract class SyncHandler<T extends AbstractMessage> implements Handler {

    private final TypeToken<T> typeToken = new TypeToken<T>(getClass()) {
    };
    private final Type type = typeToken.getType(); // or getRawType() to return Class<? super T>

    public Type getType() {
        return type;
    }

    public abstract HandlerOutput onRequest(HandlerInput<T> input);

}
