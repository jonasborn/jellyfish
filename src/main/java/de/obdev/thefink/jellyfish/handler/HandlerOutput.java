package de.obdev.thefink.jellyfish.handler;

/**
 * Created by Jonas Born on 30.04.17.
 */
public class HandlerOutput {

    byte[] bytes = null;

    public byte[] getBytes() {
        return bytes;
    }

    public HandlerOutput setBytes(byte[] bytes) {
        this.bytes = bytes;
        return this;
    }
}
