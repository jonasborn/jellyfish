package de.obdev.thefink.jellyfish.decoder.mongo;


import de.obdev.thefink.jellyfish.message.mongo.CommandReplyMessage;
import de.obdev.thefink.jellyfish.message.mongo.MessageHeader;
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage;
import de.obdev.thefink.jellyfish.message.mongo.OpCode;
import de.obdev.thefink.jellyfish.util.mongo.buffer.MongoBuffer;
import de.obdev.thefink.jellyfish.util.mongo.document.MongoDocument;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

/**
 * Created by Jonas Born on 21.04.17.
 */
public class CommandReplyMessageDecoder extends AbstractMongoMessageDecoder {
    @Override
    public OpCode[] getSupported() {
        return new OpCode[]{OpCode.COMMANDREPLY};
    }

    @Override
    public MongoMessage decode(MessageHeader header, ByteBuffer buffer) {
        MongoBuffer mongoBuffer = new MongoBuffer(buffer);
        MongoDocument metadata = null;
        try {
            metadata = mongoBuffer.getDocument();
        } catch (IOException e) {
            e.printStackTrace();
        }
        MongoDocument commandReply = null;
        try {
            commandReply = mongoBuffer.getDocument();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<MongoDocument> outputDocs = null;
        try {
            outputDocs = mongoBuffer.getDocuments();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new CommandReplyMessage(
                header,
                metadata,
                commandReply,
                outputDocs
        );
    }
}
