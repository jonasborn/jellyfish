package de.obdev.thefink.jellyfish.decoder;



import de.obdev.thefink.jellyfish.decoder.jellyfish.JellyfishDecoder;
import de.obdev.thefink.jellyfish.encoder.AbstractMessageEncoder;
import de.obdev.thefink.jellyfish.message.AbstractMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Jonas Born on 26.04.17.
 */
public class MessageDecoders {

    static Logger logger = LogManager.getLogger();

    static List<AbstractMessageDecoder> decoders = new ArrayList<>();

    static {

        Reflections reflections = new Reflections();
        Set<Class<? extends AbstractMessageDecoder>> encs =  reflections.getSubTypesOf(AbstractMessageDecoder.class);

        for (Class<? extends AbstractMessageDecoder> enc:encs) {
            if (!Modifier.isAbstract(enc.getModifiers())) {
                try {
                    AbstractMessageDecoder e = enc.newInstance();
                    decoders.add(e);
                    logger.info("Added message decoder ({})", e.getClass());
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    static public void add(AbstractMessageDecoder decoder) {
        decoders.add(decoder);
    }

    public static AbstractMessage decode(byte[] bytes) throws ClassNotFoundException {
        for (AbstractMessageDecoder decoder:decoders) {

            if (decoder.isSupported(bytes)) {
                return decoder.decode(bytes);
            }
        }
        return null;
    }

}
