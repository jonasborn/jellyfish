package de.obdev.thefink.jellyfish.decoder.mongo;


import de.obdev.thefink.jellyfish.message.mongo.MessageHeader;
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage;
import de.obdev.thefink.jellyfish.message.mongo.OpCode;
import de.obdev.thefink.jellyfish.message.mongo.QueryMessage;
import de.obdev.thefink.jellyfish.util.mongo.buffer.MongoBuffer;
import de.obdev.thefink.jellyfish.util.mongo.document.MongoDocument;

import java.nio.ByteBuffer;

/**
 * Created by Jonas Born on 20.04.17.
 */
public class QueryMessageDecoder extends AbstractMongoMessageDecoder {


    @Override
    public OpCode[] getSupported() {
        return new OpCode[]{OpCode.QUERY};
    }


    @Override
    public MongoMessage decode(MessageHeader header, ByteBuffer buffer) {

        MongoBuffer mongoBuffer = new MongoBuffer(buffer);

        Integer flags = mongoBuffer.getInt();
        String fullCollectionName = mongoBuffer.getString();
        Integer toSkip = mongoBuffer.getInt();
        Integer toReturn = mongoBuffer.getInt();
        MongoDocument query = new MongoDocument(mongoBuffer.asByteBuffer());

        return new QueryMessage(
                header,
                flags,
                fullCollectionName,
                toSkip,
                toReturn,
                query
        );

    }


}
