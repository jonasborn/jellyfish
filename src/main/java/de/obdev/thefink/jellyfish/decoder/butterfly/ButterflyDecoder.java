package de.obdev.thefink.jellyfish.decoder.butterfly;

import de.obdev.thefink.jellyfish.decoder.AbstractMessageDecoder;
import de.obdev.thefink.jellyfish.message.jellyfish.JellyfishMessage;
import de.obdev.thefink.jellyfish.message.butterfly.ButterflyMessage;
import de.obdev.thefink.jellyfish.util.baler.Baler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by Jonas Born on 27.04.17.
 */
public class ButterflyDecoder extends AbstractMessageDecoder<ButterflyMessage> {

    @Override
    public Boolean isSupported(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer.position(4);
        return buffer.get() == ((byte) 0xfe);
    }

    public ButterflyMessage decode(byte[] bytes) {
        byte[] res = new byte[bytes.length -5];
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        buffer.position(5);
        buffer.get(res);
        try {
            Object result = Baler.unpack(res);
            if (result instanceof JellyfishMessage) {
                return (ButterflyMessage) result;
            } else return null;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}
