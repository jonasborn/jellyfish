package de.obdev.thefink.jellyfish.decoder.mongo;


import de.obdev.thefink.jellyfish.message.mongo.MSGMessage;
import de.obdev.thefink.jellyfish.message.mongo.MessageHeader;
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage;
import de.obdev.thefink.jellyfish.message.mongo.OpCode;
import de.obdev.thefink.jellyfish.util.mongo.buffer.MongoBuffer;

import java.nio.ByteBuffer;

/**
 * Created by Jonas Born on 21.04.17.
 */
public class MSGMessageDecoder extends AbstractMongoMessageDecoder {
    @Override
    public OpCode[] getSupported() {
        return new OpCode[]{OpCode.MSG};
    }

    @Override
    public MongoMessage decode(MessageHeader header, ByteBuffer buffer) {
        MongoBuffer mongoBuffer = new MongoBuffer(buffer);
        String message = mongoBuffer.getString();
        return new MSGMessage(
                header, message
        );
    }

}
