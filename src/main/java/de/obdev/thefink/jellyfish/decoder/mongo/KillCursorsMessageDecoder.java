package de.obdev.thefink.jellyfish.decoder.mongo;


import de.obdev.thefink.jellyfish.message.mongo.KillCursorsMessage;
import de.obdev.thefink.jellyfish.message.mongo.MessageHeader;
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage;
import de.obdev.thefink.jellyfish.message.mongo.OpCode;

import java.nio.ByteBuffer;

/**
 * Created by Jonas Born on 21.04.17.
 */
public class KillCursorsMessageDecoder extends AbstractMongoMessageDecoder {
    @Override
    public OpCode[] getSupported() {
        return new OpCode[]{OpCode.KILL_CURSORS};
    }

    @Override
    public MongoMessage decode(MessageHeader header, ByteBuffer buffer) {
        Integer zero = buffer.getInt();
        Integer numberOfCursorIds = buffer.getInt();
        Long cursorIds = buffer.getLong();

        return new KillCursorsMessage(
                header, zero, numberOfCursorIds, cursorIds
        );

    }
}
