package de.obdev.thefink.jellyfish.decoder.mongo;


import de.obdev.thefink.jellyfish.message.mongo.CommandMessage;
import de.obdev.thefink.jellyfish.message.mongo.MessageHeader;
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage;
import de.obdev.thefink.jellyfish.message.mongo.OpCode;
import de.obdev.thefink.jellyfish.util.mongo.buffer.MongoBuffer;
import de.obdev.thefink.jellyfish.util.mongo.document.MongoDocument;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

/**
 * Created by Jonas Born on 21.04.17.
 */
public class CommandMessageDecoder extends AbstractMongoMessageDecoder {
    @Override
    public OpCode[] getSupported() {
        return new OpCode[]{OpCode.COMMAND};
    }

    @Override
    public MongoMessage decode(MessageHeader header, ByteBuffer buffer) {
        MongoBuffer mongoBuffer = new MongoBuffer(buffer);
        String database = mongoBuffer.getString();
        String commandName = mongoBuffer.getString();
        MongoDocument metadata = null;
        try {
            metadata = mongoBuffer.getDocument();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        MongoDocument commandArgs = null;
        try {
            commandArgs = mongoBuffer.getDocument();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        List<MongoDocument> inputDocs = null;
        try {
            inputDocs = mongoBuffer.getDocuments();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return new CommandMessage(
                header,
                database,
                commandName,
                metadata,
                commandArgs,
                inputDocs
        );
    }
}
