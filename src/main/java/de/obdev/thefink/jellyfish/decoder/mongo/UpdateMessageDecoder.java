package de.obdev.thefink.jellyfish.decoder.mongo;


import de.obdev.thefink.jellyfish.message.mongo.MessageHeader;
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage;
import de.obdev.thefink.jellyfish.message.mongo.OpCode;
import de.obdev.thefink.jellyfish.message.mongo.UpdateMessage;
import de.obdev.thefink.jellyfish.util.mongo.buffer.MongoBuffer;
import de.obdev.thefink.jellyfish.util.mongo.document.MongoDocument;

import java.nio.ByteBuffer;

/**
 * Created by Jonas Born on 20.04.17.
 */
public class UpdateMessageDecoder extends AbstractMongoMessageDecoder {
    @Override
    public OpCode[] getSupported() {
        return new OpCode[]{OpCode.UPDATE};
    }

    @Override
    public MongoMessage decode(MessageHeader header, ByteBuffer buffer) {
        MongoBuffer mongoBuffer = new MongoBuffer(buffer);
        Integer zero = mongoBuffer.getInt();
        String fullCollectionName = mongoBuffer.getString();
        Integer flags = mongoBuffer.getInt();
        MongoDocument selector = new MongoDocument(mongoBuffer.asByteBuffer());
        MongoDocument update = new MongoDocument(mongoBuffer.asByteBuffer());

        return new UpdateMessage(
                header,
                zero,
                fullCollectionName,
                flags,
                selector,
                update
        );
    }
}
