package de.obdev.thefink.jellyfish.decoder.mongo;


import de.obdev.thefink.jellyfish.message.mongo.MessageHeader;
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage;
import de.obdev.thefink.jellyfish.message.mongo.OpCode;
import de.obdev.thefink.jellyfish.message.mongo.ReplyMessage;
import de.obdev.thefink.jellyfish.util.mongo.buffer.MongoBuffer;
import de.obdev.thefink.jellyfish.util.mongo.document.MongoDocument;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

/**
 * Created by Jonas Born on 20.04.17.
 */
public class ReplyMessageDecoder extends AbstractMongoMessageDecoder {
    @Override
    public OpCode[] getSupported() {
        return new OpCode[]{OpCode.REPLY};
    }

    @Override
    public MongoMessage decode(MessageHeader header, ByteBuffer buffer) {

        MongoBuffer mongoBuffer = new MongoBuffer(buffer);

        Integer flags = mongoBuffer.getInt();
        Long cursorId = mongoBuffer.getLong();
        Integer from = mongoBuffer.getInt();
        Integer to = mongoBuffer.getInt();
        Integer returned = mongoBuffer.getInt();
        List<MongoDocument> documents = null;
        try {
            documents = mongoBuffer.getDocuments();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ReplyMessage(
                header,
                flags,
                cursorId,
                from,
                to,
                returned,
                documents
        );

    }
}
