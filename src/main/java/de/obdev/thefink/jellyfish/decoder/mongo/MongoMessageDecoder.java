package de.obdev.thefink.jellyfish.decoder.mongo;

import de.obdev.thefink.jellyfish.decoder.AbstractMessageDecoder;
import de.obdev.thefink.jellyfish.message.mongo.MessageHeader;
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage;
import de.obdev.thefink.jellyfish.message.mongo.OpCode;
import org.apache.commons.codec.binary.Hex;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jonas Born on 27.04.17.
 */
public class MongoMessageDecoder extends AbstractMessageDecoder<MongoMessage> {

    static Map<OpCode, AbstractMongoMessageDecoder> decoders = new HashMap<>();

    static {
        decoders.put(OpCode.COMMAND, new CommandMessageDecoder());
        decoders.put(OpCode.COMMANDREPLY, new CommandReplyMessageDecoder());
        decoders.put(OpCode.DELETE, new DeleteMessageDecoder());
        decoders.put(OpCode.GET_MORE, new GetMoreMessageDecoder());
        decoders.put(OpCode.KILL_CURSORS, new KillCursorsMessageDecoder());
        decoders.put(OpCode.MSG, new MSGMessageDecoder());
        decoders.put(OpCode.QUERY, new QueryMessageDecoder());
        decoders.put(OpCode.REPLY, new ReplyMessageDecoder());
        decoders.put(OpCode.UPDATE, new UpdateMessageDecoder());
    }



    @Override
    public Boolean isSupported(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        MessageHeader header = new MessageHeader(buffer);
        return header.getOpCode() != null;
    }

    @Override
    public MongoMessage decode(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        MessageHeader header = new MessageHeader(buffer);
        AbstractMongoMessageDecoder decoder = decoders.get(header.getOpCode());
        return decoder.decode(header, buffer);
    }
}
