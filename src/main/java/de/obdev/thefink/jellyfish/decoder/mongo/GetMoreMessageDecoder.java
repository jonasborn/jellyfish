package de.obdev.thefink.jellyfish.decoder.mongo;


import de.obdev.thefink.jellyfish.message.mongo.GetMoreMessage;
import de.obdev.thefink.jellyfish.message.mongo.MessageHeader;
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage;
import de.obdev.thefink.jellyfish.message.mongo.OpCode;
import de.obdev.thefink.jellyfish.util.mongo.buffer.MongoBuffer;

import java.nio.ByteBuffer;

/**
 * Created by Jonas Born on 20.04.17.
 */
public class GetMoreMessageDecoder extends AbstractMongoMessageDecoder {
    @Override
    public OpCode[] getSupported() {
        return new OpCode[]{OpCode.GET_MORE};
    }

    @Override
    public MongoMessage decode(MessageHeader header, ByteBuffer buffer) {
        MongoBuffer mongoBuffer = new MongoBuffer(buffer);
        Integer zero = mongoBuffer.getInt();
        String fullCollectionName = mongoBuffer.getString();
        Integer toReturn = mongoBuffer.getInt();
        Long cursorId = mongoBuffer.getLong();
        return new GetMoreMessage(
                header,
                zero,
                fullCollectionName,
                toReturn,
                cursorId
        );
    }
}
