package de.obdev.thefink.jellyfish.decoder.mongo;


import de.obdev.thefink.jellyfish.message.mongo.MessageHeader;
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage;
import de.obdev.thefink.jellyfish.message.mongo.OpCode;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jonas Born on 20.04.17.
 */
public abstract class AbstractMongoMessageDecoder {

    public abstract OpCode[] getSupported();
    

    public abstract MongoMessage decode(MessageHeader header, ByteBuffer buffer);
}
