package de.obdev.thefink.jellyfish.encoder.mongo;

import com.google.common.primitives.Ints;
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage;
import de.obdev.thefink.jellyfish.message.mongo.OpCode;
import de.obdev.thefink.jellyfish.message.mongo.QueryMessage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by Jonas Born on 21.04.17.
 */
public class QueryMessageEncoder extends MongoMessageEncoder {
    @Override
    public OpCode[] getSupported() {
        return new OpCode[]{OpCode.QUERY};
    }

    @Override
    public byte[] encode(MongoMessage message) {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        if (message instanceof QueryMessage) {
            QueryMessage queryMessage = ((QueryMessage) message);
            try {
                bout.write(Ints.toByteArray(queryMessage.getFlags()));
                bout.write(queryMessage.getFullCollectionName().getBytes());
                bout.write(0x00);
                bout.write(Ints.toByteArray(queryMessage.getToSkip()));
                bout.write(Ints.toByteArray(queryMessage.getToReturn()));
                bout.write(queryMessage.getQuery().toByteArray());
                return queryMessage.getHeader().toByteArray(bout.toByteArray());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
