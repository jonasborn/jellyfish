package de.obdev.thefink.jellyfish.encoder.butterfly;



import de.obdev.thefink.jellyfish.encoder.AbstractMessageEncoder;
import de.obdev.thefink.jellyfish.message.butterfly.ButterflyMessage;
import de.obdev.thefink.jellyfish.message.jellyfish.JellyfishMessage;
import de.obdev.thefink.jellyfish.util.baler.Baler;
import org.apache.commons.codec.binary.Hex;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by Jonas Born on 23.04.17.
 */
public class ButterflyEncoder extends AbstractMessageEncoder<ButterflyMessage> {



    @Override
    public byte[] encode(ButterflyMessage message) {
        byte[] res = new byte[0];
        try {
            res = Baler.pack(message);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteBuffer buffer = ByteBuffer.allocate(res.length + 5);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer.putInt(res.length + 5).put((byte) 0xfe).put(res);
        byte[] fin = new byte[res.length + 5];
        buffer.position(0);
        buffer.get(fin);
        return fin;
    }
}
