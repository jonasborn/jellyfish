package de.obdev.thefink.jellyfish.encoder.mongo;


import de.obdev.thefink.jellyfish.encoder.AbstractMessageEncoder;
import de.obdev.thefink.jellyfish.message.mongo.MongoMessage;
import de.obdev.thefink.jellyfish.message.mongo.OpCode;

/**
 * Created by Jonas Born on 21.04.17.
 */
public abstract class MongoMessageEncoder extends AbstractMessageEncoder<MongoMessage> {

    public abstract OpCode[] getSupported();

    public abstract byte[] encode(MongoMessage message);

}
