package de.obdev.thefink.jellyfish.encoder;

import de.obdev.thefink.jellyfish.message.AbstractMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Jonas Born on 26.04.17.
 */
public class MessageEncoders {

    static List<AbstractMessageEncoder> encoders = new ArrayList<>();

    static Logger logger = LogManager.getLogger();

    static {

        Reflections reflections = new Reflections();
        Set<Class<? extends AbstractMessageEncoder>> encs =  reflections.getSubTypesOf(AbstractMessageEncoder.class);

        for (Class<? extends AbstractMessageEncoder> enc:encs) {
            if (!Modifier.isAbstract(enc.getModifiers())) {
                try {
                    AbstractMessageEncoder e = enc.newInstance();
                    encoders.add(e);
                    logger.info("Added message encoder ({})", e.getClass());
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    public static byte[] encode(AbstractMessage message) throws ClassNotFoundException {
        for (AbstractMessageEncoder encoder:encoders) {
            Class cls = Class.forName(encoder.getType().getTypeName());
            if (cls.isAssignableFrom(message.getClass())) {
                return encoder.encode(message);
            }
        }
        return null;
    }

}
