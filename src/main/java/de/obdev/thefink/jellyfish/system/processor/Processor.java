package de.obdev.thefink.jellyfish.system.processor;



import de.obdev.thefink.jellyfish.system.SysInfos;

import java.io.Serializable;

/**
 * Created by Jonas Born on 23.04.17.
 */
public class Processor implements Serializable{

    Integer physicalProcessorCount = null;
    Long vendorFreq = null;

    Double systemCpuLoad = null;
    Long systemUptime = null;

    public Processor() {

    }



    public Integer getPhysicalProcessorCount() {
        return physicalProcessorCount;
    }

    public Long getVendorFreq() {
        return vendorFreq;
    }

    public Double getSystemCpuLoad() {
        return systemCpuLoad;
    }

    public Long getSystemUptime() {
        return systemUptime;
    }

    public Processor setPhysicalProcessorCount(Integer physicalProcessorCount) {
        this.physicalProcessorCount = physicalProcessorCount;
        return this;
    }

    public Processor setVendorFreq(Long vendorFreq) {
        this.vendorFreq = vendorFreq;
        return this;
    }

    public Processor setSystemCpuLoad(Double systemCpuLoad) {
        this.systemCpuLoad = systemCpuLoad;
        return this;
    }

    public Processor setSystemUptime(Long systemUptime) {
        this.systemUptime = systemUptime;
        return this;
    }

    public void setPhysicalProcessorCount() {
        this.physicalProcessorCount = SysInfos.getSystemInfo().getHardware().getProcessor().getPhysicalProcessorCount();
    }

    public void setVendorFreq() {
        this.vendorFreq = SysInfos.getSystemInfo().getHardware().getProcessor().getVendorFreq();
    }

    public void setSystemCpuLoad() {
        this.systemCpuLoad = SysInfos.getSystemInfo().getHardware().getProcessor().getSystemCpuLoad();
    }

    public void setSystemUptime() {
        this.systemUptime = SysInfos.getSystemInfo().getHardware().getProcessor().getSystemUptime();
    }

    @Override
    public String toString() {
        return "Processor{" +
                "physicalProcessorCount=" + physicalProcessorCount +
                ", vendorFreq=" + vendorFreq +
                ", systemCpuLoad=" + systemCpuLoad +
                ", systemUptime=" + systemUptime +
                '}';
    }
}
