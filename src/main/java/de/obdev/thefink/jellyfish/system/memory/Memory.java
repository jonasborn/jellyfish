package de.obdev.thefink.jellyfish.system.memory;


import de.obdev.thefink.jellyfish.system.SysInfos;

import java.io.Serializable;

/**
 * Created by Jonas Born on 23.04.17.
 */
public class Memory implements Serializable{

    Long total = null;
    Long available = null;
    Long swapTotal = null;
    Long swapUsed = null;

    public Long getTotal() {
        return total;
    }

    public Long getAvailable() {
        return available;
    }

    public Long getSwapTotal() {
        return swapTotal;
    }

    public Long getSwapUsed() {
        return swapUsed;
    }

    public Memory setTotal(Long total) {
        this.total = total;
        return this;
    }

    public Memory setAvailable(Long available) {
        this.available = available;
        return this;
    }

    public Memory setSwapTotal(Long swapTotal) {
        this.swapTotal = swapTotal;
        return this;
    }

    public Memory setSwapUsed(Long swapUsed) {
        this.swapUsed = swapUsed;
        return this;
    }

    public void setTotal() {
        this.total = SysInfos.getSystemInfo().getHardware().getMemory().getTotal();
    }

    public void setAvailable() {
        this.available = SysInfos.getSystemInfo().getHardware().getMemory().getAvailable();
    }

    public void setSwapTotal() {
        this.swapTotal = SysInfos.getSystemInfo().getHardware().getMemory().getSwapTotal();
    }

    public void setSwapUsed() {
        this.swapUsed = SysInfos.getSystemInfo().getHardware().getMemory().getSwapUsed();
    }

    @Override
    public String toString() {
        return "Memory{" +
                "total=" + total +
                ", available=" + available +
                ", swapTotal=" + swapTotal +
                ", swapUsed=" + swapUsed +
                '}';
    }
}
