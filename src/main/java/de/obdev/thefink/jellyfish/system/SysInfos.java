package de.obdev.thefink.jellyfish.system;

import java.util.HashMap;
import java.util.Map;
import de.obdev.thefink.jellyfish.config.JellyfishId;

/**
 * Created by Jonas Born on 23.04.17.
 */
public class SysInfos implements Runnable {

    static oshi.SystemInfo systemInfo = new oshi.SystemInfo();

    static SysInfo sysInfo = new SysInfo();

    static Map<JellyfishId, SysInfo> systems = new HashMap<>();

    static {
        sysInfo.getMemory().setTotal();
        sysInfo.getMemory().setAvailable();
        sysInfo.getMemory().setSwapTotal();
        sysInfo.getMemory().setSwapUsed();

        sysInfo.getProcessor().setPhysicalProcessorCount();
        sysInfo.getProcessor().setSystemUptime();
        sysInfo.getProcessor().setSystemCpuLoad();
        sysInfo.getProcessor().setVendorFreq();
    }

    public static SysInfo current() {
        return sysInfo;
    }

    public static SysInfo get(JellyfishId JellyfishId) {
        return systems.get(JellyfishId);
    }

    public static void set(JellyfishId JellyfishId, SysInfo sysInfo) {
        //java.lang.System.out.println(JellyfishId);
        systems.put(JellyfishId, sysInfo);
    }

    public static oshi.SystemInfo getSystemInfo() {

        return systemInfo;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            SysInfos.current().getMemory().setAvailable();
            SysInfos.current().getMemory().setSwapUsed();

            SysInfos.current().getProcessor().setSystemCpuLoad();
            SysInfos.current().getProcessor().setSystemUptime();

        }
    }
}
