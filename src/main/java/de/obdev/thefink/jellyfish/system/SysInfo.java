package de.obdev.thefink.jellyfish.system;


import de.obdev.thefink.jellyfish.system.memory.Memory;
import de.obdev.thefink.jellyfish.system.processor.Processor;

import java.io.Serializable;

/**
 * Created by Jonas Born on 23.04.17.
 */
public class SysInfo implements Serializable {

    Processor processor = new Processor();
    Memory memory = new Memory();

    public Processor getProcessor() {
        return processor;
    }

    public Memory getMemory() {
        return memory;
    }

    public SysInfo setProcessor(Processor processor) {
        this.processor = processor;
        return this;
    }

    public SysInfo setMemory(Memory memory) {
        this.memory = memory;
        return this;
    }

    @Override
    public String toString() {
        return "System{" +
                "processor=" + processor +
                ", memory=" + memory +
                '}';
    }
}
