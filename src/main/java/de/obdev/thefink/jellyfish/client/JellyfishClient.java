package de.obdev.thefink.jellyfish.client;

import de.obdev.thefink.jellyfish.config.JellyfishId;
import de.obdev.thefink.jellyfish.connection.JellyfishConnection;
import de.obdev.thefink.jellyfish.message.jellyfish.welcome.FirstConnectMessage;

import java.io.IOException;
import java.net.Socket;

/**
 * Created by Jonas Born on 27.04.17.
 */
public class JellyfishClient extends JellyfishConnection {
    public JellyfishClient(Socket socket) {
        super(socket);
    }

    public JellyfishClient(String id) throws IOException, ClassNotFoundException {
        super(null);
        JellyfishId jellyfishId = JellyfishId.fromString(id);
        System.out.println(jellyfishId.getIp());
        System.out.println(jellyfishId.getPort());
        setSocket(
                new Socket(
                        jellyfishId.getIp(), jellyfishId.getPort()
                )
        );


        new Thread(() -> {
            this.write(
                    new FirstConnectMessage(jellyfishId)
            );
        }).start();

    }

    public JellyfishClient(JellyfishId id) throws IOException {
        super(null);
        setSocket(
                new Socket(
                        id.getIp(), id.getPort()
                )
        );


        new Thread(() -> {
            this.write(
                    new FirstConnectMessage(id)
            );
        }).start();

    }
}
