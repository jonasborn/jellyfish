package de.obdev.thefink.jellyfish.client;

import de.obdev.thefink.jellyfish.connection.AbstractConnection;
import de.obdev.thefink.jellyfish.decoder.MessageDecoders;
import de.obdev.thefink.jellyfish.encoder.MessageEncoders;
import de.obdev.thefink.jellyfish.handler.Handlers;
import de.obdev.thefink.jellyfish.message.AbstractMessage;
import de.obdev.thefink.jellyfish.message.jellyfish.JellyfishMessage;
import de.obdev.thefink.jellyfish.util.socket.PackageInput;
import de.obdev.thefink.jellyfish.util.socket.PackageOutput;

import java.io.IOException;
import java.net.Socket;

/**
 * Created by Jonas Born on 27.04.17.
 */
public abstract class AbstractClient<T extends AbstractMessage> extends AbstractConnection<T> {

    public AbstractClient(Socket socket) {
        super(socket);
    }

}
