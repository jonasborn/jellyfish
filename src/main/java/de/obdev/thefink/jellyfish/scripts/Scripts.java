package de.obdev.thefink.jellyfish.scripts;


import de.obdev.thefink.jellyfish.util.file.ChangeDetector;
import groovy.lang.GroovyClassLoader;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jonas Born on 23.04.17.
 */
public class Scripts {

    static File folder = new File("scripts/");

    static List<ScriptListener> scriptListeners = new ArrayList<>();
    static Map<File, Class> loadedClasses = new HashMap<>();


    static {
        loadAll(folder);
        ChangeDetector.detect(folder, new FileAlterationListener() {
            @Override
            public void onStart(FileAlterationObserver fileAlterationObserver) {

            }

            @Override
            public void onDirectoryCreate(File file) {

            }

            @Override
            public void onDirectoryChange(File file) {

            }

            @Override
            public void onDirectoryDelete(File file) {

            }

            @Override
            public void onFileCreate(File file) {
                load(file);
            }

            @Override
            public void onFileChange(File file) {
                if (loadedClasses.containsKey(file)) {
                    notifyRemove(loadedClasses.get(file));
                }
                load(file);
            }

            @Override
            public void onFileDelete(File file) {
                if (loadedClasses.containsKey(file)) {
                    notifyRemove(loadedClasses.get(file));
                }
            }

            @Override
            public void onStop(FileAlterationObserver fileAlterationObserver) {

            }
        });


    }


    public static void addListener(ScriptListener listener) {
        if (!scriptListeners.contains(listener)) {
            scriptListeners.add(listener);

            for (Class cls : loadedClasses.values()) {
                listener.onLoad(cls);
            }
        }
    }


    private static void notifyLoad(Class cls) {
        for (ScriptListener scriptListener : scriptListeners) {
            scriptListener.onLoad(cls);
        }
    }

    public static void notifyRemove(Class cls) {
        for (ScriptListener scriptListener : scriptListeners) {
            scriptListener.onRemove(cls);
        }
    }

    public static List<File> listFiles(File folder, List<File> files) {
        for (File f : folder.listFiles()) {
            if (f.isDirectory()) {
                files.addAll(listFiles(f, files));
            } else {
                files.add(f);
            }
        }
        return files;
    }

    public static void loadAll(File folder) {
        listFiles(folder, new ArrayList<>()).forEach(Scripts::load);
    }

    static void load(File file) {
        try {
            if (file.exists()) {
                GroovyClassLoader loader = new GroovyClassLoader();
                loader.addClasspath(folder.getPath());
                Class cls = loader.parseClass(file);
                loadedClasses.put(file, cls);
                notifyLoad(cls);
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }


}
