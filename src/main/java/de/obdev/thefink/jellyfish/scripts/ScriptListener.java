package de.obdev.thefink.jellyfish.scripts;

/**
 * Created by Jonas Born on 23.04.17.
 */
public interface ScriptListener {

    public void onLoad(Class cls);

    public void onRemove(Class cls);

}
